# Integrating PHP to MariaDB

First, connect to the MariaDB console with the database user
```
mysql -u lemp_user -p
```
From the MariaDB console, run the following statement to create a table:
```
CREATE TABLE lempdb.students (ID INT NOT NULL AUTO_INCREMENT , NAME VARCHAR(20) NOT NULL , SURNAME VARCHAR(20) , ADDRESS VARCHAR(20), COMPANY VARCHAR(20) , AGE INT NOT NULL , DATE TIMESTAMP , PRIMARY KEY (ID)) ;
```
Add some data:

```
INSERT INTO lempdb.students ( NAME , SURNAME , AGE ) VALUES ( 'Ali', 'Huseynov', 30), ( 'Said', 'Huseynov', 30);
exit
```
Now you can create the PHP script that will connect to MariaDB and query for your content.
```
vi /usr/share/nginx/html/students.php
```
```
<?php
echo "<table style='border: solid 1px black;'>";
echo "<tr><th>Id</th><th>Firstname</th><th>Lastname</th></tr>";

class TableRows extends RecursiveIteratorIterator {
  function __construct($it) {
    parent::__construct($it, self::LEAVES_ONLY);
  }

  function current() {
    return "<td style='width:150px;border:1px solid black;'>" . parent::current(). "</td>";
  }

  function beginChildren() {
    echo "<tr>";
  }

  function endChildren() {
    echo "</tr>" . "\n";
  }
}

$servername = "localhost";
$username = "lemp_user";
$password = "123";
$dbname = "lempdb";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $stmt = $conn->prepare("SELECT ID, NAME, SURNAME FROM students");
  $stmt->execute();

  // set the resulting array to associative
  $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
  foreach(new TableRows(new RecursiveArrayIterator($stmt->fetchAll())) as $k=>$v) {
    echo $v;
  }
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
echo "</table>";
?>
```
Go to http://ingress.az/students.php   to see result.

![Alt text](result.JPG)

# PHP modules checks
it is possible to get pdo error during data exchange between php and mysql. 
Get logs from:
```
tail -f /var/log/nginx/ingress_error.log
```
Get enabled php modules
```
php -m
```
PDO modules check via php file
```
<?php
if (class_exists('PDO')) {
  print "PDO is installed"; 
}
else {
  print "PDO NOT installed";
}

phpinfo();
?>
```
Find php.ini and check extensions
```
php -i | grep 'php.ini'
got the php.ini location from the phpinfo() call
```
