Tomcat instances are on 56.11, Nginx is on 56.15
There are 3 different /sample project deployed on those tomcats

| instance | mgmt port | non-ssl port | ssl port |
| ------ | ------ | ------ | ------ |
| red   |     8009    |     8089    |     8449    |
| blue   |     8008    |     8088    |     8448    |
| yellow  |     8007    |     8087    |     8447    |

LB members defined in upstream .. part

vi /etc/nginx/conf.d/php74.conf

```
server {
    listen       80;
    server_name  ingress.az;

    access_log /var/log/nginx/ingress_access.log ;
    error_log /var/log/nginx/ingress_error.log ;
    server_tokens off ;

    location /sample {
    proxy_pass http://backend ;
    }
}

upstream backend {	
    server 192.168.56.11:8089;	
    server 192.168.56.11:8088;	
    server 192.168.56.11:8087;  
}
```
Selinux can prevent LB working

Go to http://ingress.az/sample to see result.

![Alt text](result.gif)