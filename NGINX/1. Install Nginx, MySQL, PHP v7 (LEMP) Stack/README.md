# Nginx install
```
yum install nginx -y
systemctl enable --now nginx
systemctl status nginx
```
![Alt text](nginx.JPG)

# Mariadb install 
```
yum install mariadb mariadb-server -y
systemctl start mariadb
```
![Alt text](mysql.JPG)

Config
```
mysql_secure_installation
Enter current password for root (enter for none):
Switch to unix_socket authentication [Y/n] y
Change the root password? [Y/n] y
New password:
Remove anonymous users? [Y/n] y
Disallow root login remotely? [Y/n] y
Remove test database and access to it? [Y/n] y
Reload privilege tables now? [Y/n] y
```
Create sample database

```
mysql -u root -p
CREATE DATABASE lempdb;
GRANT ALL ON lempdb.* TO 'lemp_user'@'localhost' IDENTIFIED BY '123' WITH GRANT OPTION;
FLUSH PRIVILEGES;
exit
```


# PHP install

`sudo yum install php-mysqlnd`

```
dnf install https://dl.fedoraproject.org/pub/epel/epel-release-latest-9.noarch.rpm
dnf install https://rpms.remirepo.net/enterprise/remi-release-9.rpm
sudo dnf module list php
```
Available versions

```
Remi's Modular repository for Enterprise Linux 9 - x86_64
Name       Stream         Profiles                         Summary
php        remi-7.4       common [d], devel, minimal       PHP scripting language
php        remi-8.0       common [d], devel, minimal       PHP scripting language
php        remi-8.1       common [d], devel, minimal       PHP scripting language
php        remi-8.2       common [d], devel, minimal       PHP scripting language
php        remi-8.3       common [d], devel, minimal       PHP scripting language
```


Lets install 7.4

```
sudo dnf module reset php
sudo dnf module enable php:remi-7.4
sudo dnf install php74 php74-php-fpm -y
```


Start php services

```
sudo systemctl enable --now php74-php-fpm
```

Create info.php under default /usr/share/nginx/html/ directory:

```
vi /usr/share/nginx/html/info.php
```
in php file write this to display php version.
```
<?php phpinfo(); ?>
```

Open php config and make changes where apache resides to nginx:
```
vi /etc/opt/remi/php74/php-fpm.d/www.conf
```

```
# Change this
; RPM: apache user chosen to provide access to the same directories as httpd
user = nginx
; RPM: Keep a group allowed to write in log dir.
group = nginx

# Note this
listen = /var/opt/remi/php74/run/php-fpm/www.sock

# Change this
; Set permissions for unix socket, if one is used. In Linux, read/write
; permissions must be set in order to allow connections from a web server.
; Default Values: user and group are set as the running user
;                 mode is set to 0660
listen.owner = nginx
listen.group = nginx
listen.mode = 0660

# Change this
; When POSIX Access Control Lists are supported you can set them using
; these options, value is a comma separated list of user/group names.
; When set, listen.owner and listen.group are ignored
listen.acl_users = nginx
```

![Alt text](<php set1.JPG>)

![Alt text](<php set2.JPG>)

Restart php74:
```
systemctl restart php74-php-fpm.service
```

Configure custom conf file

```
vi /etc/nginx/conf.d/php74.conf
```

```
server {
    listen       80;
    server_name  ingress.az;

    access_log /var/log/nginx/ingress_access.log ;
    error_log /var/log/nginx/ingress_error.log ;
    server_tokens off ;

    location / {
        root /usr/share/nginx/html/ ;
        index  index.html index.htm info.php;
        client_max_body_size 20M;
        try_files $uri $uri.html $uri/ /error/index.html;
    }

    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_pass unix:/var/opt/remi/php74/run/php-fpm/www.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }
 
}
```
```
systemctl restart nginx
```
Then make sure you can resolve server name and go to http://ingress.az/info.php

![Alt text](<php info.JPG>)

![Alt text](php.JPG)