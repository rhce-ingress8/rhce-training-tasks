# Components
1. storage 56.20
2. node1 56.21
3. node2 56.22
4. vip 56.19

# Tasks
1. iSCSI Configuration: Install and configure iSCSI on both virtual machines.
2. Install and configure a High Availability (HA) cluster.
3. Configure PostgreSQL.
4. Simulate a failure on the node1 of the HA cluster and observe the failover. And some screenshots about all steps.