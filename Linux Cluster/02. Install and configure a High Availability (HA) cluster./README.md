On all nodes install cluster components:

```
yum install corosync pacemaker pcs fence-agents lvm2-cluster resource-agents psmisc gfs2-utils -y
```

On DNS or /etc/hosts file of each node make sure dns resolve and ping between nodes: 
```
192.168.56.21    node1.ingress.local    node1
192.168.56.22    node2.ingress.local    node2
```


On all nodes Enable PCS daemon.
```
systemctl start pcsd.service
systemctl enable pcsd.service
systemctl status pcsd.service
```


Set password for hacluster user on each node and make sure password never expires and password must be same on each node. password is 12345678

```
passwd hacluster
```

Authenticate the nodes by pcs host, Run from either one of the nodes.
```
pcs cluster auth 192.168.56.21 192.168.56.22 -u hacluster -p 12345678
```

Create cluster named as ingress-cluster2.
```
pcs cluster setup --start --name ingress-cluster2 node1 node2 -force
```

Start cluster service on all nodes. This is an Empty cluster with no resources.
```
pcs cluster enable  --all
```

Check stats
```
crm_mon -r1
pcs cluster status
pcs status
pcs status corosync
```


Install fence agents on both nodes for future requirements to fence the cluster from data corruption and resource unavailability during failovers.
```
yum install fence-agents-ipmilan
```

For now in the testing environment, disable fencing on both.
```
pcs cluster verify --full
pcs property set stonith-enabled=false
pcs cluster verify --full
```


Create mount point folder in both nodes and mount the file system on Node #1.
```
mkdir /data
vgchange -ay clustervg
mount -t xfs /dev/clustervg/lv1 /data
df -h /data
```
Install and configure PostgreSQL service on both nodes.
```
sudo yum install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm
```

Make sure the pgdg repos are enabled.
```
yum repolist enabled
```

List postgresql14 packages. Install PostgreSQL packages locally in both the nodes.
```
yum list postgresql14*
sudo yum install -y postgresql14-server
```


Run initdb to /data to create the PostgreSQL cluster.
```
chown postgres: /data
su - postgres
/usr/pgsql-14/bin/initdb --pgdata=/data
/usr/pgsql-14/bin/pg_ctl -D /data -l logfile start
```


On both nodes Setup password for postgres User at node level.
`passwd postgres     #12345`

Set the password for DB user: postgres.
```
su - postgres
psql
alter user postgres with password '12345';
\q
```


Create a test user and test database.
```
su - postgres
psql
create user admin with password 'admin';
create database testdb;
grant all privileges on database testdb to admin;
\q
```
Run ip -a, note adapter name.
Create Cluster resources to new group named:postgres - Commands to create the resources:
#LVM: 
pcs resource create ha_lvm LVM volgrpname="clustervg" exclusive=true --group postgres
#File System: 
pcs resource create ha_fs Filesystem device=”/dev/clustervg/lv1” directory=”/data” fstype=”xfs” --group postgres
#VIP: 
pcs resource create VIP IPaddr2 ip=192.168.56.19 cidr_netmask=24 nic=enp0s3 --group postgres

