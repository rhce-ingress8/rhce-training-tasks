# Step 1. Iscsi target setup
On storage server create ISCSI target. Add a new disk to vm.

To see new disk without OS restart: (where # is 0,1,2..etc)
```
echo "- - -" > /sys/class/scsi_host/host#/scan
```
![Alt text](<step1 after disk add.JPG>)

On all nodes and storage, disable selinux and firewalld:
```
setenforce 0
vi /etc/selinux/config   > selinux=disabled
systemctl disable --now firewalld
```


Install targetcli on storage server:
```
yum install targetcli -y
systemctl start target.service
systemctl enable target.service
```


Create LV on new storage:

```
vgcreate vg1 /dev/sdb
lvcreate -n lv1 -l 100%FREE vg1
mkfs.xfs /dev/vg1/lv1
```
![Alt text](<step1 after lv.JPG>)

Note your vg and lv names. 

Now we need to create ISCSI links and LUN. We will use targetcli command. It will provide new shell, and it has own set of commands. 

```
targetcli
```
1. create block device
```
/>backstores/block create blckdev1 /dev/vg1/lv1
```
![Alt text](<created block device.JPG>)

2. create iqn links
```
/>cd iscsi
/iscsi>create IQN.2024-04.local.ingress:node1
/iscsi>create IQN.2024-04.local.ingress:node2
```
![Alt text](<created iscsi links.JPG>)

3. create acl for each iqn link
```
/iscsi>cd IQN.2024-04.local.ingress:node1/tpg1/acls
/iscsi/iqn.20...de1/tpg1/acls>create IQN.2024-04.local.ingress:node1

cd /iscsi/iqn.2024-04.local.ingress:node2/tpg1/acls
/iscsi/iqn.20...de2/tpg1/acls>create IQN.2024-04.local.ingress:node2
```
![Alt text](<created acl.JPG>)

4. map block device to each iqn link
```
cd /iscsi/iqn.2024-04.local.ingress:node1/tpg1/luns
create /backstores/block/blckdev1

cd /iscsi/iqn.2024-04.local.ingress:node2/tpg1/luns
create /backstores/block/blckdev1
```
![Alt text](<created luns.JPG>)

5. save and exit
```
/>saveconfig
```

We can see 3260 open port after setup.

--------------------------------------------------------
# Step 2. Initiator setup on nodes
On both nodes do following: 
```
yum install iscsi-initiator-utils -y
```
Delete existing record and add needed string in iscsi file. 
```
vim /etc/iscsi/initiatorname.iscsi   
InitiatorName=IQN.2024-04.local.ingress:node1  		#for node1
InitiatorName=IQN.2024-04.local.ingress:node2  		#for node2
```


Test 56.20 storage server
```
iscsiadm --mode discoverydb --type sendtargets --portal 192.168.56.20 --discover
```
![Alt text](<iscsi test.JPG>)

Connect node1 server to 56.20 iscsi disk via IQN link
```
iscsiadm -m node -T iqn.2024-04.local.ingress:node1 -p 192.168.56.20 -l	
```

Connect node2 server to 56.20 iscsi disk via IQN link
```
iscsiadm -m node -T iqn.2024-04.local.ingress:node2 -p 192.168.56.20 -l
```
![Alt text](<iscsi connect.JPG>)

on both servers

```
systemctl enable iscsid iscsi
systemctl restart iscsid iscsi
```

Run `lsblk` on both servers and note new disk name. For us new disk is `sdb` 

Create LV from mapped LUN.
```
vgcreate clustervg /dev/sdb
lvcreate -l 100%FREE -n lv1 clustervg
mkfs.xfs /dev/clustervg/lv1
```

# Step 3. Volume group exclusive activation 
We need to exclude cluster lv from initd management scope. 
First run lvs to find all LV-s. 

![Alt text](<lv on nodes.JPG>)

We found that there are "centos" word in name of OS Lv-s. 
We need write directive to tell initd use only these lv-s. 

On Both nodes:

```
vim /etc/lvm/lvm.conf
volume_list = [ "centos" ] 
use_lvmetad = 0
``` 

```
lvmconf --enable-halvm --services --startstopservices
```

Need to recreate initramfs and grub config

```
cp -a /boot/initramfs-$(uname -r).img $(uname -r) /boot/initramfs-$(uname -r).img $(uname -r).bak
dracut -H -f /boot/initramfs-$(uname -r).img $(uname -r)
grub2-mkconfig -o /boot/grub2/grub.cfg

# reboot after service data folder creation, eg mysql database structure. reboot before adding disk to cluster.
reboot
```

After succesful restart use lvscan command. Our cluster vg must be inactive. 

![Alt text](step3.JPG)