Add 3 client certificate with cn 
```
user1.iamengineer.az 
user2.iamengineer.az 
user3.iamengineer.az
```


On any client

# private key:
```
openssl genrsa -out user01.key.pem 2048 
openssl genrsa -out user02.key.pem 2048 
openssl genrsa -out user03.key.pem 2048
```


# CSR: CN: user1.iamengineer.az, user2.iamengineer.az user3.iamengineer.az
```
openssl req -new -key user01.key.pem -out user01.csr.pem
openssl req -new -key user02.key.pem -out user02.csr.pem
openssl req -new -key user03.key.pem -out user03.csr.pem
```
![Alt text](<user cert.JPG>)

# Copy CSR to Intermed CSR folder and Sign on Intermed server
```
cd /CA_INT/
openssl ca -config openssl.cnf -extensions usr_cert -days 375 -notext -md sha256 -in csr/user01.csr.pem -out certs/user01.cert.pem
openssl ca -config openssl.cnf -extensions usr_cert -days 375 -notext -md sha256 -in csr/user02.csr.pem -out certs/user02.cert.pem
openssl ca -config openssl.cnf -extensions usr_cert -days 375 -notext -md sha256 -in csr/user03.csr.pem -out certs/user03.cert.pem
```
# Create PFX bundle for each user cert
```
cd /CA_INT/
openssl pkcs12 -export -out certs/user1.pfx -inkey private/user01.key.pem -in certs/user01.cert.pem -certfile certs/intermediate-iamengineer.cert.pem -certfile certs/ca-iamengineer.cert.pem
openssl pkcs12 -export -out certs/user2.pfx -inkey private/user02.key.pem -in certs/user02.cert.pem -certfile certs/intermediate-iamengineer.cert.pem -certfile certs/ca-iamengineer.cert.pem
openssl pkcs12 -export -out certs/user3.pfx -inkey private/user03.key.pem -in certs/user03.cert.pem -certfile certs/intermediate-iamengineer.cert.pem -certfile certs/ca-iamengineer.cert.pem
```

![Alt text](<intermed folder structure.JPG>)

Copy generated cert files to client