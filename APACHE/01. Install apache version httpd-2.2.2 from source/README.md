# Install Development tools:

```
yum groupinstall "Development Tools"  -y  
yum install expat-devel pcre pcre-devel openssl-devel  wget git -y
```


# Download Apache version and compatible apr utils. Kudos AI

```
wget https://archive.apache.org/dist/apr/apr-1.3.12.tar.gz

wget https://archive.apache.org/dist/apr/apr-util-1.3.12.tar.gz

wget https://archive.apache.org/dist/httpd/httpd-2.2.2.tar.bz2
```


# Unzip 

```
tar -xvf httpd-2.2.2.tar.bz2

tar -xvf apr-util-1.3.12.tar.gz

tar -xvf apr-1.3.12.tar.gz
```


# Creating folder structure

```
mv apr-1.3.12/   httpd-2.2.2/srclib/apr

mv apr-util-1.3.12/     httpd-2.2.2/srclib/apr-util

cd httpd-2.2.2
```


# Configure, Make and install httpd 2.2.2

```
./configure --enable-ssl --enable-so --with-mpm=event --enable-module=mod_cgi --enable-module=mod_ssl --enable-module=mod_proxy_http --with-included-apr --prefix=/etc/httpd

#You will get `Error, SSL/TLS libraries were missing or unusable`. To resolve it:

./configure --with-ssl --enable-so --with-mpm=event --enable-module=mod_cgi --enable-module=mod_ssl --enable-module=mod_proxy_http --with-included-apr --prefix=/etc/httpd

make

make install
```


# Post installation

To see version info run  `httpd -V`
You will get `command not found` error. To solve it do next operations:

1. Add pathmunge to below file:

```
vim /etc/profile.d/httpd.sh
pathmunge /etc/httpd/bin
```
2. Add pathmunge command alias to ~/.bashrc:

```
cd ~
vim .bashrc
```
```
pathmunge () {
    if ! echo $PATH | /bin/egrep -q "(^|:)$1($|:)" ; then
        if [ "$2" = "after" ] ; then
            PATH=$PATH:$1
        else
            PATH=$1:$PATH
        fi
    fi
}
```
3. Logout or `source ~/.bashrc`   to apply 
4. Run `httpd -V` again. You should get version info

![Alt text](<httpd -v.JPG>)

5. Create service file, Save and exit the file

`vim /etc/systemd/system/httpd.service`
    
```
[Unit]
Description=The Apache HTTP Server
After=network.target

[Service]
Type=forking
ExecStart=/etc/httpd/bin/apachectl -k start
ExecReload=/etc/httpd/bin/apachectl -k graceful
ExecStop=/etc/httpd/bin/apachectl -k graceful-stop
PIDFile=/etc/httpd/logs/httpd.pid
```
```
systemctl daemon-reload
systemctl start httpd
```
If service doesn't start, solve Selinux permissions

![Alt text](status.JPG)
