We need download different versions from external source.

```
dnf install https://dl.fedoraproject.org/pub/epel/epel-release-latest-9.noarch.rpm
dnf install https://rpms.remirepo.net/enterprise/remi-release-9.rpm
sudo dnf module list php
```
```
Remi's Modular repository for Enterprise Linux 9 - x86_64
Name       Stream         Profiles                         Summary
php        remi-7.4       common [d], devel, minimal       PHP scripting language
php        remi-8.0       common [d], devel, minimal       PHP scripting language
php        remi-8.1       common [d], devel, minimal       PHP scripting language
php        remi-8.2       common [d], devel, minimal       PHP scripting language
php        remi-8.3       common [d], devel, minimal       PHP scripting language

```


Lets install 7.4

```
sudo dnf module reset php
sudo dnf module enable php:remi-7.4
sudo dnf install php74 php74-php-fpm -y
```


Repeat for 8.2 versions

```
sudo dnf module reset php
sudo dnf module enable php:remi-8.2
sudo dnf install php82 php82-php-fpm -y
```


Repeat for 8.3 versions

```
sudo dnf module reset php
sudo dnf module enable php:remi-8.3
sudo dnf install php83 php83-php-fpm -y
```


New 7.4, 8.2 and 8.3 versions are in `/etc/opt/remi/php74`  ; `/etc/opt/remi/php82` and `/etc/opt/remi/php83` folders. 
By default, PHP versions are listening on port `9000`. But we want to run three versions simultaneously. We need change this port for each Php.

```
sudo sed -i 's/:9000/:9002/'  /etc/opt/remi/php74/php-fpm.d/www.conf
sudo sed -i 's/:9000/:9003/'  /etc/opt/remi/php82/php-fpm.d/www.conf
sudo sed -i 's/:9000/:9004/'  /etc/opt/remi/php83/php-fpm.d/www.conf
```


Also make Selinux changes

```
sudo semanage port -a -t http_port_t -p tcp 9002
sudo semanage port -a -t http_port_t -p tcp 9003
sudo semanage port -a -t http_port_t -p tcp 9004
```
Start php services

`sudo systemctl enable --now php74-php-fpm php82-php-fpm php83-php-fpm`

Create info.php for each versions

```
mkdir /var/www/html/php74
mkdir /var/www/html/php82
mkdir /var/www/html/php83
```


```
vi /var/www/html/php74/info.php
vi /var/www/html/php82/info.php
vi /var/www/html/php83/info.php
```


in each php file write this to display php version. 
`<?php phpinfo(); ?>`

Change permissions for new folders, because Apache runs under apache user. 

```
sudo chown -R apache:apache /var/www/html/php74
sudo chown -R apache:apache /var/www/html/php82
sudo chown -R apache:apache /var/www/html/php83
sudo chmod -R 755 /var/www/html/php74
sudo chmod -R 755 /var/www/html/php82
sudo chmod -R 755 /var/www/html/php83
```
Now configure Apache for websites.

For php 7.4

`sudo vi /etc/httpd/conf.d/php74.conf`

```
<VirtualHost *:80>
    ServerName php74.iamengineer.az
    DocumentRoot /var/www/html/php74
    DirectoryIndex info.php
    ErrorLog /var/log/httpd/php74-error.log
    CustomLog /var/log/httpd/php74-access.log combined

    <IfModule !mod_php7.c>
        <FilesMatch \.(php|phar)$>
            SetHandler "proxy:unix:/var/opt/remi/php74/run/php-fpm/www.sock|fcgi://localhost"
        </FilesMatch>
    </IfModule>
</VirtualHost>
```


For php 8.2

`sudo vi /etc/httpd/conf.d/php82.conf`

```
<VirtualHost *:80>
    ServerName php82.iamengineer.az
    DocumentRoot /var/www/html/php82
    DirectoryIndex info.php
    ErrorLog /var/log/httpd/php82-error.log
    CustomLog /var/log/httpd/php82-access.log combined

    <IfModule !mod_php8.c>
        <FilesMatch \.(php|phar)$>
            SetHandler "proxy:unix:/var/opt/remi/php82/run/php-fpm/www.sock|fcgi://localhost"
        </FilesMatch>
    </IfModule>
</VirtualHost>
```


For php 8.3

`sudo vi /etc/httpd/conf.d/php83.conf`

```
<VirtualHost *:80>
    ServerName php83.iamengineer.az
    DocumentRoot /var/www/html/php83
    DirectoryIndex info.php
    ErrorLog /var/log/httpd/php83-error.log
    CustomLog /var/log/httpd/php83-access.log combined

    <IfModule !mod_php8.c>
        <FilesMatch \.(php|phar)$>
            SetHandler "proxy:unix:/var/opt/remi/php83/run/php-fpm/www.sock|fcgi://localhost"
        </FilesMatch>
    </IfModule>
</VirtualHost>
```
Restart Apache and go to http://php83.iamengineer.az/info.php

`systemctl  restart  httpd`

![Alt text](result.JPG)