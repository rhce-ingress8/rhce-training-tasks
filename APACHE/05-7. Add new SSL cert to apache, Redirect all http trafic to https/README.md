# SSL Redirection on Apache
To Redirect http to https on apache we need to make 2 virtual host files.

Uncomment these modules in httpd.conf to activate them 
```
LoadModule ssl_module modules/mod_ssl.so
LoadModule rewrite_module modules/mod_rewrite.so
```

Create non-ssl vhost and redirect to ssl

```
vim /etc/httpd/conf.d/vhost80.conf
								
<VirtualHost *:80>								
ServerName test.iamengineer.az					

# Enable Rewrite Redirect
RewriteEngine on
RewriteCond %{SERVER_PORT} !^443$
RewriteRule ^/(.*) https://%{HTTP_HOST}/$1 [NC,R=301,L]

</VirtualHost>
```
Create ssl vhost

```
vim vhost443.conf

 Listen 443
<VirtualHost *:443>
   SSLEngine On         #Enable SSL
   SSLCertificateFile /etc/httpd/certs/test.iamengineer.az.cert.pem			#Pub key of srv
   SSLCertificateChainFile /etc/httpd/certs/ca-chain.pem		            #Chain of trust
   SSLCertificateKeyFile /etc/httpd/certs/test.iamengineer.az.key.pem		#Priv key of srv
   SSLProtocol -all +TLSv1.2

   DocumentRoot /var/www/iamengineer
   ServerName test.iamengineer.az
</VirtualHost>
```


Copy server key and cert + chain files to /etc/httpd/certs 

Restart the service.

Create index.html file inside /var/www/iamengineer