Need to use chrome 57 version to achieve green ssl.

Update for Chrome 58+ (Released 2017-04-19)
As of Chrome 58, the ability to identify the host using only `commonName` was removed. Certificates must now use `subjectAltName` to identify their host(s). See further discussion here and bug tracker here. In the past, subjectAltName was used only for multi-host certs so some internal CA tools don't include them.

If your self-signed certs worked fine in the past but suddenly started generating errors in Chrome 58, this is why.

So whatever method you are using to generate your self-signed cert (or cert signed by a self-signed CA), ensure that the server's cert contains a subjectAltName with the proper DNS and/or IP entry/entries, even if it's just for a single host.

For openssl, this means your OpenSSL config (`/etc/ssl/openssl.cnf` on Ubuntu) should have something similar to the following for a single host:

```
[v3_ca]   # and/or [v3_req], if you are generating a CSR
subjectAltName = DNS:example.com
```


or for multiple hosts:

```
[v3_ca]   # and/or [v3_req], if you are generating a CSR
subjectAltName = DNS:example.com, DNS:host1.example.com, DNS:*.host2.example.com, IP:10.1.2.3
```
![Alt text](result.JPG)

In Chrome's cert viewer (which has moved to "Security" tab under F12) you should see it listed under Extensions as Certificate Subject Alternative Name.