# Prepare new PKI Infrastructure. 
# On rootca 56.13 

```
# private key: passwd-12345
openssl genrsa -aes256 -out private/ca-iamengineer.key.pem 4096
```


```
# public key: CN-rootca.iamengineer.az
openssl req -config openssl.cnf -key private/ca-iamengineer.key.pem -new -x509 -days 7300 -sha256 -extensions v3_ca -out certs/ca-iamengineer.cert.pem
```


# On intermed 56.12

```
# private key: passwd-1234
openssl genrsa -aes256 -out private/intermediate-iamengineer.key.pem 4096
```

```
# CSR: CN-intermed.iamengineer.az
openssl req -config  openssl.cnf -new -sha256  -key private/intermediate-iamengineer.key.pem   -out csr/intermediate-iamengineer.csr.pem
```


# Sign CSR on rootca

Copy intermed CSR to CSR folder on Root and run:

```
openssl ca -config openssl.cnf -extensions v3_intermediate_ca  -days 3650 -notext -md sha256  -in csr/intermediate-iamengineer.csr.pem  -out certs/intermediate-iamengineer.cert.pem
```

Copy generated intermed public key to intermed certs folder

# On LAMP 56.14 Create server cert

```
yum install openssl mod_ssl -y
cd /etc/pki/tls
```


```
# private key: 
openssl genrsa -out private/test.iamengineer.az.key.pem 2048
# CSR: CN-test.iamengineer.az
openssl req -config openssl.cnf  -key private/test.iamengineer.az.key.pem -new -sha256 -out test.iamengineer.az.csr.pem 
```


```
# Copy server CSR to Intermed and Sign
openssl ca -config openssl.cnf -extensions server_cert  -days 375 -notext -md sha256 -in csr/test.iamengineer.az.csr.pem -out certs/test.iamengineer.az.cert.pem
```
# Make bundle and chain

```
#Bundle
cat test.iamengineer.az.cert.pem | tee -a test.iamengineer.az.bundled.crt
cat intermediate-iamengineer.cert.pem | tee -a test.iamengineer.az.bundled.crt
cat ca-iamengineer.cert.pem | tee -a test.iamengineer.az.bundled.crt
```


```
#Chain
cat intermediate-iamengineer.cert.pem > ca-chain.pem
cat ca-iamengineer.cert.pem >> ca-chain.pem
```

