We need to configure Apache to verify users by certificate. This can be achieved by adding 3 lines to vhost443.conf file.

```
SSLVerifyClient require                 #Enabling feature
SSLVerifyDepth 2                        #HOW MANY CA and intermed certs IN CHAIN
SSLCACertificateFile /path/to/chain.pem #Location of chain file
```

vi /etc/httpd/conf.d/vhost443.conf

```
<VirtualHost *:443>
   SSLEngine On
   SSLCertificateFile /etc/httpd/certs/test.iamengineer.az.cert.pem
   SSLCertificateChainFile /etc/httpd/certs/ca-chain.pem
   SSLCertificateKeyFile /etc/httpd/certs/test.iamengineer.az.key.pem
   SSLProtocol -all +TLSv1.2

   SSLVerifyClient require
   SSLVerifyDepth 2
   SSLCACertificateFile /etc/httpd/certs/ca-chain.pem

   DocumentRoot /var/www/html/iamengineer
   ServerName test.iamengineer.az
</VirtualHost>
```

systemctl restart httpd

Now from windows go to https://test.iamengineer.az
Result will be error like: `test.iamengineer.az didnt accept your login certificate`

![Alt text](<Before add user cert.JPG>)

Now add certificate to firefox: root and intermed to authorities, user pfx to your certs.

![Alt text](<add cert to firefox.JPG>)

try again, you will be asked for user cert to accept and page will open.

![Alt text](<accept cert request firefox.JPG>)

Result:

![Alt text](<result after adding user cert.JPG>)