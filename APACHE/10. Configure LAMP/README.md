A LAMP stack is a bundle of four different software technologies that developers use to build websites and web applications. LAMP is an acronym for the operating system, Linux; the web server, Apache; the database server, MySQL; and the programming language, PHP.
# Linux

![Alt text](L.JPG)

# Install Apache to serve your content. 

```
yum install httpd -y
systemctl enable --now httpd.service
```
![Alt text](A.JPG)
 
# Install MariaDB to store and manage your data. 

```
yum install mariadb mariadb-server -y
systemctl start mariadb
```
![Alt text](M.JPG)

Config:
```
mysql_secure_installation
Enter current password for root (enter for none):
Switch to unix_socket authentication [Y/n] n
Change the root password? [Y/n] y
New password:
Remove anonymous users? [Y/n] y
Disallow root login remotely? [Y/n] y
Remove test database and access to it? [Y/n] y
Reload privilege tables now? [Y/n] y
```
Create test database and user with password `123`
```
mysql -u root -p
CREATE DATABASE example_database;
GRANT ALL ON example_database.* TO 'example_user'@'localhost' IDENTIFIED BY '123' WITH GRANT OPTION;
FLUSH PRIVILEGES;
exit
```
# Install PHP for processing code to display dynamic content to the user.

```
sudo yum install php php-mysqlnd
sudo systemctl restart httpd.service
```
![Alt text](P.JPG)

# Integrating PHP to MariaDB

First, connect to the MariaDB console with the database user

`mysql -u example_user -p`

From the MariaDB console, run the following statement to create a table:

`CREATE TABLE example_database.students (ID INT NOT NULL AUTO_INCREMENT , NAME VARCHAR(20) NOT NULL , SURNAME VARCHAR(20) , ADDRESS VARCHAR(20), COMPANY VARCHAR(20) , AGE INT NOT NULL , DATE TIMESTAMP , PRIMARY KEY (ID)) ;`

Add some data:
```
INSERT INTO example_database.students ( NAME , SURNAME , AGE ) VALUES ( 'Ali', 'Huseynov', 30), ( 'Said', 'Huseynov', 30);
exit
```


Now you can create the PHP script that will connect to MariaDB and query for your content.

`vi /var/www/html/students.php`

```
<?php
echo "<table style='border: solid 1px black;'>";
echo "<tr><th>Id</th><th>Firstname</th><th>Lastname</th></tr>";

class TableRows extends RecursiveIteratorIterator {
  function __construct($it) {
    parent::__construct($it, self::LEAVES_ONLY);
  }

  function current() {
    return "<td style='width:150px;border:1px solid black;'>" . parent::current(). "</td>";
  }

  function beginChildren() {
    echo "<tr>";
  }

  function endChildren() {
    echo "</tr>" . "\n";
  }
}

$servername = "localhost";
$username = "example_user";
$password = "123";
$dbname = "example_database";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $stmt = $conn->prepare("SELECT ID, NAME, SURNAME FROM students");
  $stmt->execute();

  // set the resulting array to associative
  $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
  foreach(new TableRows(new RecursiveArrayIterator($stmt->fetchAll())) as $k=>$v) {
    echo $v;
  }
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;
echo "</table>";
?>
```

Go to http://ip/students.php to see result

![Alt text](Result.JPG)
