# 3 types of virtual directory 
Best practices for hosting multiple virtual hosts on apache is to separate their config files. To do so, we need to create a folder and in main config include them all. 

Create folder named conf.d inside /etc/httpd. Open `/etc/httpd/conf/httpd.conf` and insert `IncludeOptional conf.d/*.conf` to end of file. Then create conf files inside `/etc/httpd/conf.d/`. You can find samples below. If there is `Listen 80` in main conf, no need to write it in conf.d folder. Otherwise you need to write `Listen <port>` on custom virtual host. After creating conf files open port on firewall, check selinux and restart apache service. 

Create `/var/www/` folder, in `httpd.conf` allow to connect from outside:
```
<Directory "/var/www">
    AllowOverride None
    # Allow open access:
    Require all granted
</Directory>
```
Create folders `www.mydomain1.com`,`www.mydomain2.com` and `www.mydomain3.com` inside `/var/www/`, then `index.html` in each of them. 


1. Domain based. This method uses ServerName parameter for separating each virtual host on same nic interface and port:

```
Listen 80
<VirtualHost *:80>							
    DocumentRoot /var/www/www.mydomain1.com		
    ServerName mydomain1.com							
</VirtualHost>
```

```
Listen 80
<VirtualHost *:80>							
    DocumentRoot /var/www/www.mydomain2.com		
    ServerName mydomain2.com							
</VirtualHost>
```
![Alt text](2._Domain_Based.JPG)

2. Port based. This method uses different port numbers for each virtual host on same nic interface. 

```
Listen 81
<VirtualHost *:81>							
    DocumentRoot /var/www/www.mydomain1.com		
    ServerName mydomain1.com							
</VirtualHost>
```

```
Listen 82
<VirtualHost *:82>							
    DocumentRoot /var/www/www.mydomain2.com		
    ServerName mydomain2.com							
</VirtualHost>
```
![Alt text](2._Port_Based.JPG)

3. Ip based. This method uses different nic interfaces for each virtual host on same server. 

```
Listen 80
<VirtualHost 192.168.56.14:80>							
    DocumentRoot /var/www/www.mydomain1.com		
    ServerName mydomain1.com							
</VirtualHost>
```


```
Listen 80
<VirtualHost 192.168.56.105:80>							
    DocumentRoot /var/www/www.mydomain3.com		
    ServerName mydomain3.com							
</VirtualHost>
```

![Alt text](2._Ip_Based.JPG)