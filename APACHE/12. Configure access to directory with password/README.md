Create 2 users with passwords on Apache
```
htpasswd -c /etc/httpd/.passwords murad
htpasswd /etc/httpd/.passwords farid
```
Create needed Directory structure
```
vi /var/www/html/index.html
mkdir /var/www/html/protected
vi /var/www/html/protected/index.html
```
Create virtual host with Protected Page

`vi /etc/httpd/conf.d/vhost80.conf`

```
<VirtualHost *:80>
        DocumentRoot /var/www/html
        ServerName test.iamengineer.az
        ServerAdmin admin@iamengineer.az

        Alias /protected /var/www/html/protected


<Directory /var/www/html/protected>
        AuthType Basic
        AuthName "Protected Page"
        AuthUserFile /etc/httpd/.passwords
        Require user murad
</Directory>

</VirtualHost>
```
`Require valid-user` grants all users in apache to login, but `Require user username` grants only selected user.

![Alt text](result.gif)