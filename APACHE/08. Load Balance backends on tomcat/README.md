Tomcat instances are on 56.11, Apache is on 56.14
There are 3 different /sample project deployed on those tomcats

| instance | mgmt port | non-ssl port | ssl port |
| ------ | ------ | ------ | ------ |
| red   |     8009    |     8089    |     8449    |
| blue   |     8008    |     8088    |     8448    |
| yellow  |     8007    |     8087    |     8447    |

Enable these modules in httpd.conf:
```
LoadModule proxy_module modules/mod_proxy.so
LoadModule proxy_http_module modules/mod_proxy_http.so
LoadModule proxy_balancer_module modules/mod_proxy_balancer.so
LoadModule slotmem_shm_module modules/mod_slotmem_shm.so
LoadModule lbmethod_byrequests_module modules/mod_lbmethod_byrequests.so
```
Then create Virtual Host like below. But remove comments.

vim /etc/httpd/conf.d/vhost80.conf
```
							
<VirtualHost *:80>					
   DocumentRoot /var/www/iamengineer
   ServerName test.iamengineer.az
	
<Proxy    "balancer://sample" >							# balancer name
    BalancerMember  "http://192.168.56.11:8087/sample"	# members
    BalancerMember  "http://192.168.56.11:8088/sample"
    BalancerMember  "http://192.168.56.11:8089/sample"
</Proxy>
    ProxyPass "/lb"         "balancer://sample"				# proxy modu
    ProxyPassReverse "/lb"  "balancer://sample"

</VirtualHost>
```

Last, on browser go `http://test.iamengineer/lb`

![Alt text](result.gif)