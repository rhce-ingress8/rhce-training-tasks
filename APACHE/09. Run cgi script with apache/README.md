CGI scripts feature allows us run commands in the Apache server console and get result via web. 

1. Add custom conf file.

`vi /etc/httpd/conf.d/vhost80.conf`

```
<VirtualHost *:80>
   DocumentRoot /var/www/iamengineer
   ServerName test.iamengineer.az

# Query to call script
   Alias /cgi /var/www/html/scripts

# Define scripts directory
<Directory /var/www/html/scripts>
        Options +ExecCGI                    # Enable CGI
        SetHandler cgi-script               # Module for CGI
        AddHandler cgi-script .cgi .pl      # runnable extensions
        Require all granted                 # security options
</Directory>
</VirtualHost>
```
2. Create script file. Save and give execute permission to file.

```
mkdir -p /var/www/html/scripts
vim /var/www/html/scripts/script.cgi
```


```
#!/bin/bash
echo -e "Content-type: text/html\n\n"
# needed command here
echo `ss -tulpn | grep LIST`
```


`chmod +x /var/www/html/scripts/script.cgi`

Enable cgi module in httpd.conf

`LoadModule cgid_module modules/mod_cgid.so`

3. Restart the httpd service and Go to link :

http://test.iamengineer.az/cgi/script.cgi

![Alt text](result.JPG)