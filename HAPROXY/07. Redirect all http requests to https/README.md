```
frontend first
 mode http
 bind *:80
 redirect scheme https code 301 if !{ ssl_fc }
 bind *:443 ssl crt /etc/haproxy/ssl/firsthaproxy-bundle.pem crl-file /etc/haproxy/ssl/iamengineer-intermediate.crl.pem
 use_backend first if { req.hdr(host) -i firsthaproxy.az }

backend first
 balance roundrobin
 server app1 192.168.56.14:8081 check
```
