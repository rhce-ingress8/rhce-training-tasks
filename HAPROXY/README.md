Tomcat instances are on 56.11, Haproxy is on 56.16
There are 3 different /sample project deployed on those tomcats

| instance | mgmt port | non-ssl port | ssl port |
| ------ | ------ | ------ | ------ |
| red   |     8009    |     8089    |     8449    |
| blue   |     8008    |     8088    |     8448    |
| yellow  |     8007    |     8087    |     8447    |


Tasks

1. Install Haproxy
2. Configure 2 frontends and 2 backends
3. Install socat

- [ ] use this command and send screen `echo "show info"|socat stdio /var/lib/haproxy/stats`
4. Sign 2 server certificate `“firsthaproxy.az”` `“secondhaproxy.az”` and 2 client certificate `“hapclient1”` and `“hapclient2”` . Send certificates screen on windows
5. Configure frontends with ssl and crl send green bar without errors
6. Revoke one of the client certs and send result on browser
7. Redirect all http requests to https
8. Configure stats page with password auth
9. Configure basic auth
- [ ] with unsecure password
- [ ] with encrypted password
10. Add ACL for PATH BEGIN, PATH END , Source address, domin name, and configure correct, appropriate backend
11. Configure tcp load balancing ( with check)
12. Install second Haproxy
- [ ] Copy config from first haproxy
- [ ] Edit same configs if required
13. Install the keepalived on both servers
- [ ] select one virtual ip and configure keepalived
- [ ] if one of the servers shuts down (or does not see each other on the network), the virtual ip must switch to another
- [ ] check the status of the proxy, if the result is unsuccessful, switch to another virtual ip.
- [ ] Load balance with new ip virtual address