# Haproxy config on first node.

```
global
    log         127.0.0.1 local2

    chroot      /var/lib/haproxy
    pidfile     /var/run/haproxy.pid
    maxconn     4000
    user        haproxy
    group       haproxy
    daemon

    # turn on stats unix socket
    stats socket /var/lib/haproxy/stats

    # utilize system-wide crypto-policies
    ssl-default-bind-ciphers PROFILE=SYSTEM
    ssl-default-server-ciphers PROFILE=SYSTEM

defaults
    mode                    http
    log                     global
    option                  httplog
    option                  dontlognull
    option http-server-close
    option forwardfor       except 127.0.0.0/8
    option                  redispatch
    retries                 3
    timeout http-request    10s
    timeout queue           1m
    timeout connect         10s
    timeout client          1m
    timeout server          1m
    timeout http-keep-alive 10s
    timeout check           10s
    maxconn                 3000



frontend ecomm
        bind *:80
        mode http
        use_backend tomcats if { hdr(host) -i ecomm.iamengineer.az }

frontend landing
        bind *:80
        mode http
        http-request redirect prefix http://%[hdr(host),regsub(^www\.,,i)] code 301 if { hdr_beg(host) -i www. }
        acl welcome hdr(host) -i iamengineer.az
        use_backend landing if welcome

backend landing
        option httpchk
        mode http
        server lamp 192.168.56.14:8181 check inter 20s

backend tomcats
    balance  leastconn
    server  red 192.168.56.11:8089 check
    server  blue 192.168.56.11:8088 check
    server  yellow 192.168.56.11:8087 check
```


# Apache config on LAMP server

```
Listen 8181
<VirtualHost *:8181>
        DocumentRoot /var/www/html/haproxy
        ServerName iamengineer.az
	    ServerName www.iamengineer.az
        ServerAdmin admin@iamengineer.az

</VirtualHost>
```
To test apache, edit hosts file

```
192.168.56.14	ecomm.iamengineer.az www.iamengineer.az iamengineer.az
```
![Alt text](<8181 apache working.JPG>)

To test haproxy01 

```
192.168.56.16	ecomm.iamengineer.az www.iamengineer.az iamengineer.az
```
![Alt text](<web working via haproxy.JPG>)

# Second Haproxy node is 56.17. 

Install haproxy and copy haproxy.cfg into /etc/haproxy/
```
yum install haproxy.x86_64 -y
```
To test haproxy02

```
192.168.56.17	ecomm.iamengineer.az www.iamengineer.az iamengineer.az
```

![Alt text](<web working via haproxy2.JPG>)
