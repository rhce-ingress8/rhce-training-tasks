We have 2 mariadb server: 56.11 and 56.12.
DB named `task` is configured for replication between nodes. 
Username is `senior`, password is `12345`.
Master is 56.11.


MariaDB [task]> select * from students;

| ID | NAME   | SURNAME  | ADDRESS | COMPANY | AGE | DATE                |
|----|--------|----------|---------|---------|-----|---------------------|
|  1 | Ali    | Huseynov | Baku    | STP     |  30 | 2024-02-05 06:17:02 |
|  2 | Hasan  | Aliyev   | NULL    | NULL    |  30 | 2024-02-07 16:46:49 |
|  3 | Ali    | Huseyn   | NULL    | NULL    |  30 | 2024-02-08 06:48:38 |
|  4 | Said   | Huseyn   | NULL    | NULL    |  30 | 2024-02-08 06:48:38 |
|  5 | Mammad | Huseyn   | NULL    | NULL    |  30 | 2024-02-08 07:16:47 |
|  6 | Said   | Hasanov  | NULL    | NULL    |  30 | 2024-02-08 07:16:47 |

Now login to one of the MariaDB server and create user for HAProxy 56.16

```
CREATE USER 'haproxy_check'@'192.168.56.16'; FLUSH PRIVILEGES;
GRANT ALL PRIVILEGES ON *.* TO 'haproxy_root'@'192.168.56.16' IDENTIFIED BY 'secret' WITH GRANT OPTION; FLUSH PRIVILEGES;
```

On Haproxy 56.16

`vi /etc/haproxy/haproxy.cfg`

```
global
    log 127.0.0.1 local0 notice
    user haproxy
    group haproxy
defaults
    log global
    retries 2
    timeout connect 3000
    timeout server 5000
    timeout client 5000
listen mysql-cluster
    bind 192.168.56.16:3306
    mode tcp
    option mysql-check user haproxy_check
    balance roundrobin
    server mysql-1 192.168.56.11:3306 check
    server mysql-2 192.168.56.12:3306 check
```


Start HAProxy, consider firewall and selinux
```
setsebool -P haproxy_connect_any=1
systemctl start haproxy
```
![Alt text](<status current.JPG>)

Test connection to MariaDB servers through HAProxy

`mysql -u haproxy_root -p -h 192.168.56.16`

![Alt text](mysql-over-haproxy.JPG)

![Alt text](<current connection.JPG>)

Stop one of the MariaDB server service.

![Alt text](<status after one node down.JPG>)

You should still be able to connect to MariaDB through HAProxy

![Alt text](<mssql after node down.JPG>)

![Alt text](<connection after node down.JPG>)