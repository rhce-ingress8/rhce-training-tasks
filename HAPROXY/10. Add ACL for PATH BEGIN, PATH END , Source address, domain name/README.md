Add ACL for PATH BEGIN, PATH END , Source address, domain name

On 56.14 Apache server we have static websites. Also on 56.11 there are tomcats with /sample apps. Links are:
```
http://php74.iamengineer.az/test.png
http://firsthaproxy.az:8081
http://192.168.56.11:8089
```


# ACL for PATH BEGIN
```
frontend begin
        bind *:80
        mode http
        acl path_sample path -i -m beg /sample 			
        use_backend tomcat  if path_sample

backend tomcat
        server  red 192.168.56.11:8089 check
```




# ACL for PATH END
```
frontend end
        bind *:80
        mode http
        use_backend php if { path_end .png }

backend php
        option httpchk
        mode http
        server lamp 192.168.56.14:80 check inter 20
```
s




# ACL for Source address
```
frontend source
        mode http
        bind *:80
        acl  ip_list src -f /etc/haproxy/iplist.txt
        use_backend source if ip_list

backend source
        balance roundrobin
        server app2 192.168.56.14:8082 check
```




# ACL for Domain name
```
frontend domain
        mode http
        bind *:80
        use_backend domain if { hdr(host) -m dom firsthaproxy.az }

backend domain
        balance roundrobin
        server app1 192.168.56.14:8081 check
```



