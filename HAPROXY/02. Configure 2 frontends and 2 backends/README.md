First, lets configure logs to `/var/log/haproxy.log`

# HAPROXY logs via Rsyslog on local

```
vim /etc/rsyslog.d/haproxy.conf
```

```
$ModLoad imudp
$UDPServerRun 514
$UDPServerAddress 127.0.0.1
local2.* /var/log/haproxy.log
```

Save and reload rsyslog

```
systemctl restart rsyslog
ss -tulpn | grep 514
```

Edit haproxy.conf. Under default paste this line to make it default for all frontends.

```
vi /etc/haproxy/haproxy.cfg
```
```
log-format	{\"haproxy_clientIP\":\"%ci\",\"haproxy_clientPort\":\"%cp\",\"haproxy_dateTime\":\"%t\",\"haproxy_frontendNameTransport\":\"%ft\",\"haproxy_backend\":\"%b\",\"haproxy_serverName\":\"%s\",\"haproxy_Tw\":\"%Tw\",\"haproxy_Tc\":\"%Tc\",\"haproxy_Tt\":\"%Tt\",\"haproxy_bytesRead\":\"%B\",\"haproxy_terminationState\":\"%ts\",\"haproxy_actconn\":%ac,\"haproxy_FrontendCurrentConn\":%fc,\"haproxy_backendCurrentConn\":%bc,\"haproxy_serverConcurrentConn\":%sc,\"haproxy_retries\":%rc,\"haproxy_srvQueue\":%sq,\"haproxy_backendQueue\":%bq,\"haproxy_backendSourceIP\":\"%bi\",\"haproxy_backendSourcePort\":\"%bp\",\"haproxy_statusCode\":\"%ST\",\"haproxy_serverIP\":\"%si\",\"haproxy_serverPort\":\"%sp\",\"haproxy_frontendIP\":\"%fi\",\"haproxy_frontendPort\":\"%fp\",\"haproxy_capturedRequestHeaders\":\"%hr\",\"haproxy_httpRequest\":\"%hr\",\"HTTP_request_URI\":,\"HTTP_request_URI_query_string\":,\"Server_Name\":\"%s\",\"Request_Headers\}
```

# Website with ecomm module

We have a static website with Ecommerce module link in page. Website is www.iamengineer.az, 
Ecommerce link is ecomm.iamengineer.az. Ecomm module consist of 3 tomcats. I created a clickable button with ecomm.iamengineer.az link inside index.html of static page home directory in 56.14 apache server.

First we go to www.iamengineer.az, Haproxy redirects it to landing backend. When we click on button we go to http://ecomm.iamengineer.az/sample, which is redirected to tomcats backend.

```
vi /etc/haproxy/conf.d/website.cfg
```
```
global
    log         127.0.0.1 local2

    chroot      /var/lib/haproxy
    pidfile     /var/run/haproxy.pid
    maxconn     4000
    user        haproxy
    group       haproxy
    daemon

    # turn on stats unix socket
    stats socket /var/lib/haproxy/stats

    # utilize system-wide crypto-policies
    ssl-default-bind-ciphers PROFILE=SYSTEM
    ssl-default-server-ciphers PROFILE=SYSTEM

defaults
    mode                    http
    log                     global
    option                  httplog
    option                  dontlognull
    option http-server-close
    option forwardfor       except 127.0.0.0/8
    option                  redispatch
    retries                 3
    timeout http-request    10s
    timeout queue           1m
    timeout connect         10s
    timeout client          1m
    timeout server          1m
    timeout http-keep-alive 10s
    timeout check           10s
    maxconn                 3000



frontend ecomm
        bind *:80
        mode http
        use_backend tomcats if { hdr(host) -i ecomm.iamengineer.az }

frontend landing
        bind *:80
        mode http
        http-request redirect prefix http://%[hdr(host),regsub(^www\.,,i)] code 301 if { hdr_beg(host) -i www. }
        acl welcome hdr(host) -i iamengineer.az
        use_backend landing if welcome

frontend php
        bind *:80
        mode http
        use_backend landing if { hdr(host) -i php74.iamengineer.az }
        use_backend landing if { hdr(host) -i php82.iamengineer.az }
        use_backend landing if { hdr(host) -i php83.iamengineer.az }

backend landing
        option httpchk
        mode http
        server lamp 192.168.56.14:80 check inter 20s

backend tomcats
    balance  leastconn
    server  red 192.168.56.11:8089 check
    server  blue 192.168.56.11:8088 check
    server  yellow 192.168.56.11:8087 check


```
To check config, use:
```
haproxy -c -f /etc/haproxy/conf.d/website.cfg
```
Save and restart haproxy
```
systemctl restart haproxy.service
```

![Alt text](landing_backend.gif)


![Alt text](stats.JPG)