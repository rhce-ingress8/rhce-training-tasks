# On 56.14 LAMP server create 2 web pages
vi /etc/httpd/conf.d/secondhaproxy.az.conf

```
Listen 8082
<VirtualHost *:8082>							
    DocumentRoot /var/www/secondhaproxy.az		
    ServerName secondhaproxy.az							
</VirtualHost>
```


vi /etc/httpd/conf.d/firsthaproxy.az.conf

```
Listen 8081
<VirtualHost *:8081>							
    DocumentRoot /var/www/firsthaproxy.az		
    ServerName firsthaproxy.az							
</VirtualHost>
```


```
mkdir /var/www/firsthaproxy.az
mkdir /var/www/secondhaproxy.az
systemctl start httpd
```


```
vi /var/www/firsthaproxy.az/index.html
vi /var/www/secondhaproxy.az/index.html
```




# On 56.16 haproxy
 Edit cfg file under /etc/haproxy

 vi /etc/haproxy/haproxy.cfg

```
frontend second
 mode http
 bind *:80
 redirect scheme https code 301 if !{ ssl_fc }
 bind *:443 ssl crt /etc/haproxy/ssl/secondhaproxy-bundle.pem crl-file /etc/haproxy/ssl/iamengineer-intermediate.crl.pem
 use_backend second if { req.hdr(host) -i secondhaproxy.az }

backend second
 balance roundrobin
 server app2 192.168.56.14:8082 check

frontend first
 mode http
 bind *:80
 redirect scheme https code 301 if !{ ssl_fc }
 bind *:443 ssl crt /etc/haproxy/ssl/firsthaproxy-bundle.pem crl-file /etc/haproxy/ssl/iamengineer-intermediate.crl.pem
 use_backend first if { req.hdr(host) -i firsthaproxy.az }

backend first
 balance roundrobin
 server app1 192.168.56.14:8081 check
```
Copy bundles and crl file in /etc/haproxy/ssl/

haproxy -c -f /etc/haproxy/haproxy.cfg

![Alt text](result.gif)