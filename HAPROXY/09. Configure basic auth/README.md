under `default`
```
#user list name declare
userlist myusers

#plain text pass
user murad  insecure-password 12345

#md5 hash of pass 123
user farid password $1$t70d9kec$pbT9.Bg/gUstSk7LMuJL..		
```


under needed `backend` or `frontend`

```
http-request auth unless { http_auth(myusers) }
```
![Alt text](pwd.JPG)
![Alt text](result.gif)