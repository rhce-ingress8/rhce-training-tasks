Sign 2 server certificate “firsthaproxy.az” “secondhaproxy.az” and 2 client certificate “hapclient1” and “hapclient2” . Send certificates screen on windows

# On Haproxy 56.16

```
yum install openssl mod_ssl -y
cd /etc/pki/tls
```


```
# server private key: 
openssl genrsa -out private/firsthaproxy.az.key.pem 2048
openssl genrsa -out private/secondhaproxy.az.key.pem 2048

# user private key:
openssl genrsa -out private/hapclient1.key.pem 2048
openssl genrsa -out private/hapclient2.key.pem 2048
```


# server CSR
```
# CSR: CN-firsthaproxy.az
openssl req -config openssl.cnf  -key private/firsthaproxy.az.key.pem -new -sha256 -out firsthaproxy.az.csr.pem 
# CSR: CN-secondhaproxy.az
openssl req -config openssl.cnf  -key private/secondhaproxy.az.key.pem -new -sha256 -out secondhaproxy.az.csr.pem 
```


# user CSR
```
# CSR: CN: hapclient1.firsthaproxy.az, hapclient2.firsthaproxy.az
openssl req -new -key private/hapclient1.key.pem -out hapclient1.csr.pem
openssl req -new -key private/hapclient2.key.pem -out hapclient2.csr.pem
```


# Copy CSR to Intermed CSR folder and Sign on Intermed server
```
# server certificate
cd /CA_INT/
openssl ca -config openssl.cnf -extensions server_cert  -days 375 -notext -md sha256 -in csr/firsthaproxy.az.csr.pem -out certs/firsthaproxy.az.cert.pem
openssl ca -config openssl.cnf -extensions server_cert  -days 375 -notext -md sha256 -in csr/secondhaproxy.az.csr.pem -out certs/secondhaproxy.az.cert.pem
```

```
# user certificate
cd /CA_INT/
openssl ca -config openssl.cnf -extensions usr_cert -days 375 -notext -md sha256 -in csr/hapclient1.csr.pem -out certs/hapclient1.cert.pem
openssl ca -config openssl.cnf -extensions usr_cert -days 375 -notext -md sha256 -in csr/hapclient2.csr.pem -out certs/hapclient2.cert.pem
```


# Create PFX bundle for each user cert
```
cd /CA_INT/
openssl pkcs12 -export -out certs/hapclient1.pfx -inkey private/hapclient1.key.pem -in certs/hapclient1.cert.pem -certfile certs/intermediate-iamengineer.cert.pem -certfile certs/ca-iamengineer.cert.pem
openssl pkcs12 -export -out certs/hapclient2.pfx -inkey private/hapclient2.key.pem -in certs/hapclient2.cert.pem -certfile certs/intermediate-iamengineer.cert.pem -certfile certs/ca-iamengineer.cert.pem
```
# Create bundle for Haproxy server certs

The chain hierarchy of the certificates needs to go upside down in the PEM file, so:

1. The Certificate for your domain
2. The intermediates in ascending order to the Root CA
3. A Root CA, if any (usually none)
4. Private Key

```
cat certs/firsthaproxy.az.cert.pem certs/intermediate-iamengineer.cert.pem certs/ca-iamengineer.cert.pem private/firsthaproxy.az.key.pem > firsthaproxy-bundle.pem
cat certs/secondhaproxy.az.cert.pem certs/intermediate-iamengineer.cert.pem certs/ca-iamengineer.cert.pem private/secondhaproxy.az.key.pem > secondhaproxy-bundle.pem
```
# Generate CRL
`openssl ca -config openssl.cnf -gencrl -out crl/iamengineer-intermediate.crl.pem`

![Alt text](certs.JPG)

![Alt text](crl.JPG)