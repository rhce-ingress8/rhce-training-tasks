# Keepalived service
To integrate frontend servers that only handle user requests via configuration, not depending on change of any internal server data, and can be replaced by restoring from a backup when they fail, into a high availability mode, the Keepalived service is used. 

This service operates on the Virtual IP (VIP) principle. At any given time, the VIP is active on one of the nodes, and traffic flows to that node. Virtual ip is non-local entity. It is actually not written into net adapter ipv4 settings, it floats over nodes. Traffic is directed to that VIP from the firewall. The nodes are clones of each other, differing only by their individual IP addresses.

- Keepalived config is:
`/etc/keepalived/keepalived.conf`

- testing config:
`keepalived -t`

- Installation: 
On both servers install keepalived

```
yum install keepalived.x86_64 -y
```
0. Inventory:

Our load balancer setup consists of 2 CIS benchmark hardened Redhat servers located in DMZ zone. VIP is 10.33.33.100, Nodes are 10.33.33.101 and 10.33.33.102

Keepalived service can use simple ping or script method to decide VIP floating. If we use script method, we need to verify service status inside linux. To allow keepalived run scripts need to give permissions.

1. Create user. This command creates a system user (-r) with no login shell (-s /bin/false).
```
sudo useradd -r -s /bin/false keepalived_script
 ```
2. Create health checker script. Need to create script only in `/usr/libexec/keepalived/` location due to Selinux restrictions. 
```
vim /usr/libexec/keepalived/chk_haproxy.sh
```
```
#!/bin/bash
/usr/bin/killall -0 haproxy
exit $?
```
or
```
#!/bin/bash
/usr/sbin/pidof haproxy
exit $?
```
3. Give permission to this file:
```
chown keepalived_script:keepalived_script /usr/libexec/keepalived/chk_haproxy.sh
chmod 700 /usr/libexec/keepalived/chk_haproxy.sh
```
4. Allow VIP floating on kernel level.

We need to enable this feature by using `net.ipv4.ip_nonlocal_bind = 1` parameter. Setting it to 1 allows applications to bind to non-local IP addresses, meaning an application can listen on an IP address that is not assigned to any network interface on the system.

On both servers:
```
vi /etc/sysctl.conf
```
```
net.ipv4.ip_nonlocal_bind = 1
net.ipv4.conf.all.arp_ignore = 1
net.ipv4.conf.all.arp_announce = 1
```
To apply setting:
```
sysctl -p
```
5. Give permission for haproxy to open any ports:
```
sudo setsebool -P keepalived_connect_any 1
sudo setsebool -P haproxy_connect_any 1
```
6. Keepalived config: Master - Slave status is controlled by defining `priority` and `state` in config. 
On Master priority must be greater than Slave, state must be MASTER, but on slave state is BACKUP. All other parameters are same. 

- MASTER config:
```
vrrp_script chk_haproxy {
    script "/usr/libexec/keepalived/chk_haproxy.sh"   # Check if HAProxy process is running
    interval 2                        # Check every 2 seconds
    user keepalived_script	      # script runner
}

vrrp_instance VI_1 {
    state MASTER
    interface ens33                 # Network interface to bind to
    virtual_router_id 51
    priority 101                    # Higher priority on the primary server
    advert_int 1
    authentication {
        auth_type PASS
        auth_pass yourpass          # Set a strong authentication password
    }
    virtual_ipaddress {
        10.33.33.100                # Your VIP address
    }
    track_script {
        chk_haproxy
    }
}

global_defs {
    enable_script_security	    # permit script running
    router_id haproxy01 	    # The hostname of this host
}
```
- BACKUP config:
```
vrrp_script chk_haproxy {
    script "/usr/libexec/keepalived/chk_haproxy.sh"   # Check if HAProxy process is running
    interval 2                        # Check every 2 seconds
    user keepalived_script	      # script runner
}

vrrp_instance VI_1 {
    state BACKUP
    interface ens33                 # Network interface to bind to
    virtual_router_id 51
    priority 100                    # Higher priority on the primary server
    advert_int 1
    authentication {
        auth_type PASS
        auth_pass yourpass          # Set a strong authentication password
    }
    virtual_ipaddress {
        10.33.33.100                # Your VIP address
    }
    track_script {
        chk_haproxy
    }
}

global_defs {
    enable_script_security	    # permit script running
    router_id haproxy02 	    # The hostname of this host
}
```
7. Firewall permission

- Allow VRRP Protocol
```
sudo firewall-cmd --permanent --add-rich-rule='rule protocol value="vrrp" accept'
```
- Allow VRRP Multicast Traffic
```
sudo firewall-cmd --permanent --add-rich-rule='rule family="ipv4" destination address="224.0.0.18" accept'
```
- Reload firewalld to apply changes
```
sudo firewall-cmd --reload
```
- Add custom haproxy ports to firewall
```
firewall-cmd --add-port=80/tcp --permanent
firewall-cmd --add-port=80/tcp
firewall-cmd --add-port=443/tcp --permanent
firewall-cmd --add-port=443/tcp
```
8. Check and start service
```
keepalived -t
systemctl enable --now keepalived.service
```
9. Troubleshooting. Start/stop haproxy service and check:
```
ip a
journalctl -u keepalived  
tail -f /var/log/haproxy.log
```
![Alt text](<Keepalived result.gif>)