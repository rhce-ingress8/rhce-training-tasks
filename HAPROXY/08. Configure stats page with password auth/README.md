Open haproxy.cnf and at the end of file add:


```
listen stats
        bind  *:8084
        mode http
        log global
        stats enable
        stats hide-version
        stats show-node
        stats uri /haproxy-stats
        stats auth admin:123
        stats admin if TRUE
        stats refresh 20s
```

Then systemctl restart haproxy 

Go to http://192.168.56.16:8084/haproxy-stats link to display status page.

`stats admin if TRUE` directive enables administrative actions on stats page. 

![Alt text](result.gif)