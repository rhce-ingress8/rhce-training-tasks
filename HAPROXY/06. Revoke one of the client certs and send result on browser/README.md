To achieve verifying user certificates from server side, need to include root and intermed crl files, server bundle certificates inside config.

We have created bundle files in task 4, and applied them on task 5. Now we will verify user certs.

Create CRL on root and intermed servers, then combine files. 
```
openssl ca -config openssl.cnf -gencrl -out crl/root.crl.pem
openssl ca -config openssl.cnf -gencrl -out crl/intermediate.crl.pem
cat root.crl.pem intermediate.crl.pem > combo.crl.pem
```


Copy combined file to ssl folder and edit config

vi /etc/haproxy/haproxy.cfg

```
frontend second
 mode http
 bind *:80
 redirect scheme https code 301 if !{ ssl_fc }
 bind *:443 ssl crt /etc/haproxy/ssl/secondhaproxy-bundle.pem ca-file /etc/haproxy/ssl/intermediate-iamengineer.cert.pem ca-verify-file /etc/haproxy/ssl/ca-iamengineer.cert.pem crl-file /etc/haproxy/ssl/combo.crl.pem verify required
 use_backend second if { req.hdr(host) -i secondhaproxy.az }

backend second
 balance roundrobin
 server app2 192.168.56.14:8082 check

frontend first
 mode http
 bind *:80
 redirect scheme https code 301 if !{ ssl_fc }
 bind *:443 ssl crt /etc/haproxy/ssl/firsthaproxy-bundle.pem ca-file /etc/haproxy/ssl/intermediate-iamengineer.cert.pem ca-verify-file /etc/haproxy/ssl/ca-iamengineer.cert.pem crl-file /etc/haproxy/ssl/combo.crl.pem verify required
 use_backend first if { req.hdr(host) -i firsthaproxy.az }

backend first
 balance roundrobin
 server app1 192.168.56.14:8081 check
```
**ca-verify-file**
This setting designates a PEM file from which to load CA certificates used to verify client's certificate. It designates CA certificates which must not be included in CA names sent in server hello message. Typically, "ca-file" must be defined with intermediate certificates, and "ca-verify-file" with certificates to ending the chain, like root CA.

Add recently created user certs to browser and check:

![Alt text](before.gif)

Now revoke one of user certificates, regenerate and recombine crl files
```
openssl ca -config openssl.cnf -revoke certs/hapclient1.cert.pem
openssl ca -config openssl.cnf -gencrl -out crl/intermediate.crl.pem
cat root.crl.pem intermediate.crl.pem > combo.crl.pem
systemctl restart haproxy.service
```
![Alt text](after.gif)