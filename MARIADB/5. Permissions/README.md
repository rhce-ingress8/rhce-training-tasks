5. Create 3 users : 
- Grant All permision to one 
- Grant only view permision to second, and connect only from localhost
- Grant select update delete permisions to third
- Allow the same user to connect from 2 different IPs, and give those users different CRUD premissions. Connect and check which role will be applied to user.



```
CREATE USER senior identified by '12345';
GRANT ALL ON *.* TO senior;
```

```
+----------------------------------------------------------------------------------------------------------------+
| Grants for senior@%                                                                                            |
+----------------------------------------------------------------------------------------------------------------+
| GRANT ALL PRIVILEGES ON *.* TO `senior`@`%` IDENTIFIED BY PASSWORD '*00A51F3F48415C7D4E8908980D443C29C69B60C9' |
+----------------------------------------------------------------------------------------------------------------+
```



```
CREATE USER 'audit'@'localhost' identified by '12345';	
GRANT USAGE, SELECT ON *.* TO 'audit'@'localhost';
```

```
+---------------------------------------------------------------------------------------------------------------+
| Grants for audit@localhost                                                                                    |
+---------------------------------------------------------------------------------------------------------------+
| GRANT SELECT ON *.* TO `audit`@`localhost` IDENTIFIED BY PASSWORD '*00A51F3F48415C7D4E8908980D443C29C69B60C9' |
+---------------------------------------------------------------------------------------------------------------+
```



`GRANT UPDATE, DELETE, SELECT  ON task.* TO junior IDENTIFIED BY '12345';`
```
+-------------------------------------------------------------------------------------------------------+
| Grants for junior@%                                                                                   |
+-------------------------------------------------------------------------------------------------------+
| GRANT USAGE ON *.* TO `junior`@`%` IDENTIFIED BY PASSWORD '*00A51F3F48415C7D4E8908980D443C29C69B60C9' |
| GRANT SELECT, UPDATE, DELETE ON `task`.* TO `junior`@`%`                                              |
+-------------------------------------------------------------------------------------------------------+
```



```
GRANT CREATE, SELECT, UPDATE, INSERT , DELETE  ON *.* TO 'cruder'@'localhost' IDENTIFIED BY '12345';
GRANT SELECT, DELETE  ON *.* TO 'cruder'@'192.168.56.12' IDENTIFIED BY '12345';
```


