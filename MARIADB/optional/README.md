1. Configure Master and Slave databases
2. Making changes to the master and observing the replication on the slave

# on master 192.168.56.11

enable the binary log for MariaDB instance, give uniq id to server:

`vi /etc/my.cnf`
```
[mariadb]
log-bin
server_id=1				#id of server
log-basename=master1
binlog-format=mixed
replicate-do-db=task			#db to be replicated
bind-address=192.168.56.11		#listening interface of vm
```


create user and grant replication in mariadb:

```
CREATE USER 'replication_user'@'%' IDENTIFIED BY '12345';
GRANT REPLICATION SLAVE ON *.* TO 'replication_user'@'%';
FLUSH PRIVILEGES;
```


restart the service:
`systemctl restart mariadb.service`

you need prevent any changes to the data while you view the binary log position.
```
flush and lock all tables by running:
FLUSH TABLES WITH READ LOCK;
```

Keep this session running - exiting it will release the lock.

Get the current position in the binary log by running:
```
SHOW MASTER STATUS;
+--------------------+----------+--------------+------------------+
| File               | Position | Binlog_Do_DB | Binlog_Ignore_DB |
+--------------------+----------+--------------+------------------+
| master1-bin.000003 |      344 |              |                  |
+--------------------+----------+--------------+------------------+
```


Note position and file name.

Backup databases and transfer to slave (from another shell):
`mysqldump --all-databases --user=root --password --master-data > masterdatabase.sql`

after backup on mariadb:
`UNLOCK TABLES;`

# on slave 192.168.56.12

Install mariadb and initial configure:
```
yum install mariadb mariadb-server -y
systemctl start mariadb
mysql_secure_installation
```


give unique id:
`vi /etc/my.cnf`
[mysqld]
```
server-id = 2
replicate-do-db=task
bind-address=192.168.56.12
read-only=1
```



restart the service:
`systemctl restart mariadb.service`

restore databases for initial seeding:
```
mysql -u root -p < masterdatabase.sql
systemctl restart mariadb
```


configure replication:
`mysql -u root -p`

```
> STOP SLAVE;
> CHANGE MASTER TO MASTER_HOST='192.168.56.11', MASTER_USER='replication_user', MASTER_PASSWORD='12345', MASTER_LOG_FILE='master1-bin.000003', MASTER_LOG_POS=344;
> START SLAVE;
> SHOW SLAVE STATUS \G;
```


make sure that `MASTER_LOG_FILE` matches the file and `MASTER_LOG_POS` the position returned by the earlier `SHOW MASTER STATUS`.
If replication is working correctly, both the values of Slave_IO_Running and Slave_SQL_Running should be Yes:
```
Slave_IO_Running: Yes
Slave_SQL_Running: Yes
```
