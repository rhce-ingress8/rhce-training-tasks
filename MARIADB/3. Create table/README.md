3. Create Database table with
3.1. ID uniq , auto increment ,int
3.2. Any 4 column that is 20 characters long type string
3.3. Colum which automatically writes the current date-time

```
> CREATE DATABASE task ;
> USE task ;
> CREATE TABLE students (ID INT NOT NULL AUTO_INCREMENT , NAME VARCHAR(20) NOT NULL , SURNAME VARCHAR(20) , ADDRESS VARCHAR(20), COMPANY VARCHAR(20) , AGE INT NOT NULL , DATE TIMESTAMP , PRIMARY KEY (ID)) ;
> SHOW TABLES ;
```

```
+----------------+
| Tables_in_task |
+----------------+
| students       |
+----------------+
```


`> DESCRIBE students;`
```
+---------+-------------+------+-----+---------------------+-------------------------------+
| Field   | Type        | Null | Key | Default             | Extra                         |
+---------+-------------+------+-----+---------------------+-------------------------------+
| ID      | int(11)     | NO   | PRI | NULL                | auto_increment                |
| NAME    | varchar(20) | NO   |     | NULL                |                               |
| SURNAME | varchar(20) | YES  |     | NULL                |                               |
| ADDRESS | varchar(20) | YES  |     | NULL                |                               |
| COMPANY | varchar(20) | YES  |     | NULL                |                               |
| AGE     | int(11)     | NO   |     | NULL                |                               |
| DATE    | timestamp   | NO   |     | current_timestamp() | on update current_timestamp() |
+---------+-------------+------+-----+---------------------+-------------------------------+
```


`> INSERT INTO students ( NAME , SURNAME , ADDRESS , COMPANY , AGE ) VALUES ( 'Ali', 'Huseynov', 'Baku', 'STP', 30) ;`

`> SELECT * FROM students ;`
```
+----+------+----------+---------+---------+-----+---------------------+
| ID | NAME | SURNAME  | ADDRESS | COMPANY | AGE | DATE                |
+----+------+----------+---------+---------+-----+---------------------+
|  1 | Ali  | Huseynov | Baku    | STP     |  30 | 2024-02-05 06:17:02 |
+----+------+----------+---------+---------+-----+---------------------+
```


