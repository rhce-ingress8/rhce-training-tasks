Step 1 - ID version

`mysql --version`

Step 2 - Stop DB

`sudo systemctl stop mariadb`

Step 3 - Restart DB without permission check

```
sudo mysqld_safe --skip-grant-tables --skip-networking &
mysql -u root
```


Step 4 - Changing root passwd

```
FLUSH PRIVILEGES;
ALTER USER 'root'@'localhost' IDENTIFIED BY 'new_password';			#if ID > 10.1.20
UPDATE mysql.user SET authentication_string = PASSWORD('new_password') WHERE User = 'root' AND Host = 'localhost';		#if ID > 10.1.20, if ALTER fails
SET PASSWORD FOR 'root'@'localhost' = PASSWORD('new_password');			#if ID < 10.1.20
```


Step 5 - Restart DB normally

```
sudo kill `/var/run/mariadb/mariadb.pid`
sudo systemctl start mariadb
mysql -u root -p
```
