```
yum install php php-mysqlnd
yum install httpd -y
```


# EPEL REPO

```
wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-9.noarch.rpm
yum install ./epel-release-latest-*.noarch.rpm    
    
sudo yum install phpmyadmin 
```


# CFG

`vim /etc/httpd/conf.d/phpMyAdmin.conf`

```
<Directory /usr/share/phpMyAdmin/>
   AddDefaultCharset UTF-8

   Require all granted
```

 

--------------------------------------
`vim /etc/httpd/conf/httpd.conf`
 
```
<IfModule dir_module>
 
    DirectoryIndex index.html index.php
 
</IfModule>
```


```
systemctl enable --now httpd
firewall-cmd --add-service=http --permanent
```
