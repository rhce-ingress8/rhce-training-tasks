Tasks

1. Install mariadb mariadb-server
2. Install phpMyAdmin and configure
3. Create Database table with
3.1. ID uniq , auto increment ,int
3.2. Any 4 column that is 20 characters long type string
3.3. Colum which automatically writes the current date-time
4. Send result screen from phpMyAdmin
5. Create 3 users : 
5.1 Grant All permision to one 
5.2 Grant only view permision to second, and connect only from localhost
5.3 Grant select update delete permisions to third
5.4 Allow the same user to connect from 2 different IPs, and give those users different CRUD premissions. Connect and check which role will be applied to user.
6. Reset mariadbd root password
Optional:
1. Configure Master and Slave databases
2. Making changes to the master and observing the replication on the slave