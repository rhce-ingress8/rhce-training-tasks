Creating TOMCAT_BASE directory and directory for java

#Preparing JAVA_HOME Folder
---------------------------
```
mkdir /opt/java_versions
cd /opt/java_versions
wget https://download.java.net/java/GA/jdk19.0.1/afdd2e245b014143b62ccb916125e3ce/10/GPL/openjdk-19.0.1_linux-x64_bin.tar.gz
tar -xf openjdk-19.0.1_linux-x64_bin.tar.gz
mv jdk-19.0.1/ jdk19
```


#Creating user for Tomcat instance
----------------------------------
```
useradd tomcat01
passwd tomcat01
```


#Preparing TOMCAT_BASE Folder
-----------------------------
```
mkdir /opt/tomcat
chown -R tomcat01 /opt/*
cd /opt/tomcat/
wget https://dlcdn.apache.org/tomcat/tomcat-9/v9.0.85/bin/apache-tomcat-9.0.85.tar.gz
tar -xf apache-tomcat-9.0.85.tar.gz
```


#Setting variables
------------------
```
su - tomcat01
vim .bash_profile
```
```
JAVA_HOME=/opt/java_versions/jdk19
PATH=$PATH:$JAVA_HOME/bin
export PATH
```


#Deploying test war app
-----------------------

upload war file into `/opt/tomcat/apache-tomcat-9.0.85/webapps/`
```
cd /opt/tomcat/apache-tomcat-9.0.85/bin/
./shutdown.sh
./startup.sh
```

go to http://192.168.56.11:8080/Lab6A