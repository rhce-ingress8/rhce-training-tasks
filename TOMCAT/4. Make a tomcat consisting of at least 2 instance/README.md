Biz /opt/tomcat/ folderi icerisinde 9.0.85 versiyasini apache-tomcat-9.0.85 folderi seklinde extract etmishik. 
indi bu folderi basqa adla kopyalayaq
```
cp -r apache-tomcat-9.0.85 apache-tomcat
cd apache-tomcat
```


CATALINA_HOME folderini duzeldirik. Bunun ucun bin ve lib-den basqa her seyi silirik.

`rm -rf conf/ logs/ work/ webapps/ temp/ `

INSTANCE_BASE folderini duzeldirik:
proyektlerin sayina gore instances folderi daxilinde her proyekti adi ile yaradaq:

```
[tomcat01@master apache-tomcat]$ mkdir -p instances/payment
[tomcat01@master apache-tomcat]$ mkdir -p instances/catalog
[tomcat01@master apache-tomcat]$ mkdir -p instances/price
```


`/opt/tomcat/` icerisinde yuklediyimiz tomcat arxivini bu yeni proyekt folderlerinin her birinin icine extract edek
bu folderlerin her birinin birbasa altinda tomcat file strukturu olmalidir. 

```
cd /opt/tomcat/
mkdir tomcat-template
tar -xf apache-tomcat-9.0.85.tar.gz -C tomcat-template
cd tomcat-template/apache-tomcat-9.0.85/
rm -rf R* BUILDING.txt CONTRIBUTING.md LICENSE NOTICE
cp -r /opt/tomcat/tomcat-template/apache-tomcat-9.0.85/* /opt/tomcat/apache-tomcat/instances/payment/
cp -r /opt/tomcat/tomcat-template/apache-tomcat-9.0.85/* /opt/tomcat/apache-tomcat/instances/price/
cp -r /opt/tomcat/tomcat-template/apache-tomcat-9.0.85/* /opt/tomcat/apache-tomcat/instances/catalog/
```


Indi bizim 3 eded tomcatimiz var. bunlar eyni port uzerinde isleye bilmez. Onlarin portlarini deyismek lazimdir.
tomcatin 3 dene default portu var, onlari her proyekt ucun `conf/server.xml`-den duzeldek:
```
<Server port="8005" 
<Connector port="8080"
redirectPort="8443"

cd /opt/tomcat/apache-tomcat/instances/
vi catalog/conf/server.xml		8009.8089.8449
vi payment/conf/server.xml		8008.8088.8448
vi price/conf/server.xml		8007.8087.8447
```


Indi CATALINA_HOME folderinde her 3 instansi idare ede bilmek ucun startup skript yaradaq. 
```
cd /opt/tomcat/apache-tomcat/
vi startup.sh
```


```
#!/bin/bash

instance_name=$1		#arqument atacagiq

TOMCAT_BASE=/opt/tomcat/
CATALINA_HOME=$TOMCAT_BASE/apache-tomcat
INSTANCE_BASE=$CATALINA_HOME/instances
CATALINA_BASE=$INSTANCE_BASE/$instance_name

export CATALINA_HOME CATALINA_BASE

$CATALINA_BASE/bin/startup.sh
```


skripti runnable edek
`chmod +x startup.sh`

`netstat -tulpn | grep LIST`		# ile aciq portlara baxaq

indi ise proyektleri bir bir run edek
```
cd /opt/tomcat/apache-tomcat/
./startup.sh catalog
./startup.sh price
./startup.sh payment
```


Indi ise shutdown skripti yaradaq
```
cp startup.sh shutdown.sh
vi shutdown.sh
```

```
$CATALINA_BASE/bin/shutdown.sh
chmod +x shutdown.sh
```


indi ise proyektleri bir bir shut edek
```
cd /opt/tomcat/apache-tomcat/
./shutdown.sh catalog
./shutdown.sh price
./shutdown.sh payment
```


Eger ferqli java ve tomcat versiyalari istifade etmek lazimdirsa onda her proyekte user yaradanda userin home folderini proyektin folderine beraber etmek olar.  