#!/bin/bash

instance_name=$1

TOMCAT_BASE=/opt/tomcat/
CATALINA_HOME=$TOMCAT_BASE/apache-tomcat
INSTANCE_BASE=$CATALINA_HOME/instances
CATALINA_BASE=$INSTANCE_BASE/$instance_name

export CATALINA_HOME CATALINA_BASE

$CATALINA_BASE/bin/shutdown.sh
