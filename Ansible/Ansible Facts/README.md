# Tasks

1. Create fact.yml playbook that gathers facts using the setup module and then prints some of the gathered information: Cpu model, ip interfaces, fqdn and hostname, sda1 disk size, OS Family,Distro

2. Create file called real-info.txt in control node with lines : bios version: sda size: total memory: kernel version:

3. Create task1.yml for copy this file to all host /opt/report.txt and change this lines for every node with nodes real informations with nodes real informations

4. Write a playbook to gather network interface information from remote hosts and display the hostname and IP addresses.

5. Create playbook for print inventory hostnames.

---------------
ansible_facts is a variable, which all target system information is gathered and applied to this variable on each run. It has hierarhical structure and must be used considering this. 

`ansible_facts['default_ipv4']['address']` can also be written `ansible_facts.default_ipv4.address`

We can disable it in playbook using `gather_facts: no` directive. By default setup module uses this variable to apply results. 

To get list of facts run on desired system:
```
ansible prod -m setup
```
# TASK 1

Create fact.yml playbook that gathers facts using the setup module and then prints some of the gathered information: Cpu model, ip interfaces, fqdn and hostname, sda1 disk size, OS Family,Distro
```
vim fact.yml
```
```
---
- name: display info
  hosts: test
  tasks:
    - name: cpu model
      debug:
        var: ansible_facts['processor']
    - name: ip interfaces
      debug:
        var: ansible_facts['default_ipv4']['interface']
    - name: fqdn
      debug:
        var: ansible_facts['fqdn']
    - name: hostname
      debug:
        var: ansible_facts['hostname']
    - name: sda1 disk size
      debug:
        var: ansible_facts['devices']['sda']['partitions']['sda1']['size']
    - name: OS Family
      debug:
        var: ansible_facts['os_family']
    - name: Distro
      debug:
        var: ansible_facts['distribution']
```
```
ansible-playbook fact.yml
```

![Alt text](<1. result.JPG>)

# TASK 2
Create file called real-info.txt in control node with lines : bios version: sda size: total memory: kernel version:
```
touch real-info.txt
[sandy@ans-controller ansible]$ cat real-info.txt
bios version:
sda size:
total memory:
kernel version:
```

# TASK 3
Create task1.yml for copy this file to all host /opt/report.txt and change this lines for every node with nodes real informations with nodes real informations

```
---
- name: Copy txt to nodes and update it
  hosts: all
  tasks:
    - name: Copy file from control to /opt/report.txt on each node
      copy:
        src: /home/sandy/ansible/real-info.txt
        dest: /opt/report.txt

    - name: Update Bios Info
      ansible.builtin.lineinfile:
        path: /opt/report.txt
        search_string: 'bios version:'
        line: "bios version is {{ ansible_facts['bios_version'] }}"

    - name: Update SDA size
      ansible.builtin.lineinfile:
        path: /opt/report.txt
        search_string: 'sda size:'
        line: "sda size is {{ ansible_facts['devices']['sda']['size'] }}"

    - name: Update Total Memory
      ansible.builtin.lineinfile:
        path: /opt/report.txt
        search_string: 'total memory:'
        line: "total memory is {{ ansible_facts['memtotal_mb'] }}"

    - name: Update Kernel version
      ansible.builtin.lineinfile:
        path: /opt/report.txt
        search_string: 'kernel version:'
        line: "kernel version is {{ ansible_facts['kernel_version'] }}"

```
Check result

![Alt text](<3. result.JPG>)


# TASK 4
Write a playbook to gather network interface information from remote hosts and display the hostname and IP addresses.

```
---
- name: gather ip and hostname
  hosts: all
  tasks:
    - name: gather hostnames and  all ip addresses
      debug:
        msg:
           - "INTERFACE NAME: {{ ansible_default_ipv4['interface'] }}"
           - "IP ADRESSES : {{ ansible_all_ipv4_addresses}}"
           - "HOSTNAME : {{ansible_hostname}}"

```

![Alt text](<4. result.JPG>)

# TASK 5
Create playbook for print inventory hostnames.

```
---
- name: Print Inventory Hostnames
  hosts: all
  tasks:
    - name: Print hostname
      debug:
        msg:
        - "INVENTORY NAME  {{ inventory_hostname }}"
        -  "HOSTNAME  {{ ansible_hostname }}"

```

![Alt text](<5. result.JPG>)