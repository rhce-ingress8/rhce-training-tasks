Ansible Task 6
1. Create a playbook that meets following requirements:
- Installs tmux on all targets
- Installs tcpdump and lsof on database group
- Installs mailx and lsof on proxy group

2. Create a playbook that meets following requirements: (use sysctl module)
- Is placed at system_control.yml
- Runs against all hosts
- If a server has more than 1024MB of RAM, then use sysctl to set vm.swppiness to 10
- If a server has less or equal to 1024MB of RAM exist with error message Server
has less than required 1024MB of RAM
- Configuration change should survive server reboots

3. 
- Installs the php and mariadb packages on hosts in the test, and prod host
- Installs the Development Tools package group on hosts in the dev host group
- Updates all packages to the latest version on hosts in the dev host group and
only Centos distros

4. Remove httpd package all hosts in tha conditions:
- distro RedHat or free memory bigger than 800

5. Create a playbook that meets following requirements:
- install and enable crond , sshd , httpd , firewalld with loops
