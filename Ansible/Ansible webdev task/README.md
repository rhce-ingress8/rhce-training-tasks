Create a playbook called webdev.yml in home/sandy/ansible. The playbook will create a directory /webdev on dev host. The permission of the directory are 2755 and owner is webdev. Create a symbolic link from /webdev to /var/www/html/webdev. Serve a file from webdev/index.html which displays the text "Development" curl http://node1/webdev/index.html to test


```
---
- name: Webdev Task
  hosts: dev
  tasks:
    - name: User add Webdev
      user:
        name: webdev

    - name: Create Directory /webdev
      file:
        path: /webdev
        state: directory
        mode: '2755'
        owner: webdev

    - name: Install semanage package
      yum:
        name: policycoreutils-python
        state: latest

    - name: Create index page for apache
      copy:
        content: Development
        dest: /webdev/index.html

    - name: Selinux permission for /webdev
      community.general.sefcontext:
        target: '/webdev(/.*)?'
        setype: httpd_sys_rw_content_t
        state: present


    - name: Apply new SELinux file context to filesystem
      command: restorecon -irv /webdev

    - name: Install Apache
      yum:
        name: httpd
        state: latest

    - name: Create a symbolic link
      file:
        src: /webdev
        dest: /var/www/html/webdev
        owner: webdev
        state: link

    - name: Start service httpd, if not started
      service:
        name: httpd
        state: started

    - name: Add Service httpd to firewalld
      firewalld:
        service: http
        permanent: yes
        state: enabled
        immediate: yes

```

![Alt text](result.JPG)

Curl result:

![Alt text](2.JPG)