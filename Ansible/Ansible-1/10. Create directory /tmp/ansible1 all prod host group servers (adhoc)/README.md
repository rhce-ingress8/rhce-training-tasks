
Module is ansible.builtin.file
```
- name: Create a directory if it does not exist
  ansible.builtin.file:
    path: /etc/some_directory
    state: directory
    mode: '0755'
```

Create directory /tmp/ansible1 all prod host group servers (adhoc)
```
ansible prod -m file -a 'path=/tmp/ansible1 state=directory mode='0755''
```

![Alt text](run.JPG)

Check:
```
ansible prod -m shell -a 'ls -l /tmp/'
```
![Alt text](check.JPG)