Ensure a service is started on all webservers (adhoc)

for example, enable  firewalld service:
```
ansible webservers -m ansible.builtin.service -a 'name=firewalld enabled=yes state=started'
```

![Alt text](run.JPG)

check
```
ansible webservers -m shell -a 'systemctl status firewalld'
```
![Alt text](status.JPG)