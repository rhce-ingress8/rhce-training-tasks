Config file:

![Alt text](<cfg file.JPG>)

On Ansible server create RSA keys :   
```
ssh-keygen
```

your public key is in your home dir: 
`.ssh/id_rsa.pub`

copy your id to target servers:
```
for i in 31 32 33 34 35 ; do ssh-copy-id 192.168.56.$i ; done
for i in 1 2 3 4 5 ; do ssh-copy-id node$i ; done
```
Test passwordless login:
`ssh 'node1'`

![Alt text](<ssh keys.JPG>)