Create a file called adhoc.sh in /home/sandy/ansible which will use adhoc commands to set up a new repository. The name of the repo will be 'EPEL REPO’ the description 'RHEL8 TEST REPO' the base URL Is 'https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm' the GPG key is http://repo.mysql.com/RPM-GPG-KEY-mysql, disable gpg check but not enable the repo.

```
cd /home/sandy/ansible
vi adhoc.sh
#!/bin/bash
ansible  all -m yum_repository -a 'name="EPEL_REPO" baseurl=https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm description="RHEL8 TEST REPO" gpgcheck=no gpgkey=http://repo.mysql.com/RPM-GPG-KEY-mysql enabled=no'

chmod +x adhoc.sh
./adhoc.sh
```
![Alt text](<script run.JPG>)

Check:
```
ansible all -m command -a 'cat /etc/yum.repos.d/EPEL_REPO.repo'
```
![Alt text](result.JPG)