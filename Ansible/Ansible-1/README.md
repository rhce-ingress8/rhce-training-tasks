# Environment
Centos7

| IP | server |
| ------ | ------ |
|    56.30    |    controller    |
|    56.31    |    node1    |
|    56.32    |    node2    |
|    56.33    |    node3    |
|    56.34    |    node4    |
|    56.35    |    node5    |

# Tasks

1. Install and configure ansible

2. User sandy has been created on your control node with the appropriate permissions already, do not change or modify ssh keys. Install the necessary packages to run ansible on the control node. Configure ansible.cfg

3. To be in folder /home/sandy/ansible/ansible.cfg and configure to access remote machines via the sandy user.

4. All roles should be in the path /home/sandy/ansible/roles. The inventory path should be in /home/sandy/ansible/inventory.

5. You will have access to 5 nodes. node1.example.com, node2.example.com, node3.example.com, node4.example.com, node5.example.com

6. Configure these nodes to be in an inventory file where node1 is a member of group dev. node2 is a member of group test, node3 is a member of group proxy, node4 and node 5 are members of group prod. Also, prod is a member of group webservers.

7. Create a file called adhoc.sh in/home/sandy/ansible which will use adhoc commands to set up a new repository. The name of the repo will be 'EPEL REPO’ the description 'RHEL8 TEST REPO' the base URL Is 'https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rmp' the GPG key is http://repo.mysql.com/RPM-GPG-KEY-mysql, disable gpg check but not enable the repo.

8. Install nano package in all server whith ansible adhoc

9. Ensure a service is started on all webservers (adhoc)

10. Create directory /tmp/ansible1 all prod host group servers (adhoc)

11. Write all open ports to /tmp/listen_ports all proxy host group server (adhoc