```
[defaults]
inventory = /home/sandy/ansible/inventory
remote_user = sandy
ask_pass = false
forks = 7
roles_path = /home/sandy/ansible/roles
#collections_path = /path/to/collections

[privilege_escalation]
become = true
become_method = sudo
become_user = root
become_ask_pass = false
```

