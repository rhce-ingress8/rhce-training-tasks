# On Control node Add user which will run ansible:

```
useradd sandy
passwd sandy
su - sandy
```


# Configure ansible.cfg

Most privileged ansible config is variable-declared one. So we create variable for it:

```
vim .bash_profile
ANSIBLE_CONFIG=/home/sandy/ansible/ansible.cfg
export ANSIBLE_CONFIG
```


Then create ansible.cfg in that folder:

```
[sandy@ans-controller ~]$ #
mkdir /home/sandy/ansible/
vi /home/sandy/ansible/ansible.cfg
```


```
[defaults]
inventory = /home/sandy/ansible/inventory
remote_user = sandy
ask_pass = false
forks = 7
roles_path = /home/sandy/ansible/roles
#collections_path = /path/to/collections

[privilege_escalation]
become = true
become_method = sudo
become_user = sandy
become_ask_pass = false
```


# On all nodes create user sandy with sudoers permission and nopasswd 

```
ssh root@192.168.56.35
sudo useradd -m sandy && echo 'sandy ALL=(ALL) NOPASSWD:ALL' | sudo EDITOR='tee -a' visudo
passwd sandy
```
