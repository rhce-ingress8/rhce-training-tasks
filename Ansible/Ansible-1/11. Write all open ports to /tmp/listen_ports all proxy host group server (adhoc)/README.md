Write all open ports to /tmp/listen_ports all proxy host group server (adhoc)

```
ansible proxy -m shell -a 'echo `ss -tulpn` | grep LIST > /tmp/listen_ports'
```
![Alt text](run.JPG)

```
ansible proxy -a 'cat /tmp/listen_ports'
```

![Alt text](chk.JPG)