Configure nodes to be in an inventory file where node1 is a member of group dev. node2 is a member of group test, node3 is a member of group proxy, node4 and node 5 are members of group prod. Also, prod is a member of group webservers.

```
vim /home/sandy/ansible/inventory
[dev]
node1.example.com

[test]
node2.example.com

[proxy]
node3.example.com

[prod]
node4.example.com
node5.example.com

[webservers:children]
prod
```
List all inventory
`ansible-inventory --list`

![Alt text](list.JPG)


Test connection to hosts:
`ansible all -m ping`

![Alt text](ping.JPG)