First lets find name of ansible yum module, then its arguments:
```
ansible-doc --list | grep yum
ansible-doc yum
```

```
- name: Install the latest version of Apache from the testing repo
  yum:
    name: httpd
    enablerepo: testing
    state: present
```
Now make adhoc command:
```
ansible all -m yum -a 'name=nano state=present'

```

![Alt text](adhoc.JPG)

Check if nano is installed:
```
ansible all -m shell -a 'rpm -qa | grep nano'
```
![Alt text](nano.JPG)