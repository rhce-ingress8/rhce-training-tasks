# Installation
To install Ansible run below commands

```
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py
sudo python -m pip install ansible
```

Edit /etc/hosts file and add hostnames:

```
192.168.56.31 node1 node1.example.com
192.168.56.32 node2 node2.example.com
192.168.56.33 node3 node3.example.com
192.168.56.34 node4 node4.example.com
192.168.56.35 node5 node5.example.com
```

![Alt text](version.JPG)