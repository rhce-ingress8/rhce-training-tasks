# Install Ansible-Navigator. Execute all playbooks using Ansible-Navigator.
Add repo from Redhat:
```
subscription-manager register --username usr --password pwd --auto-attach
```
Install needed components
```
yum install podman
sudo dnf install python3-pip
python3 -m pip install ansible-navigator --user
echo 'export PATH=$HOME/.local/bin:$PATH' >> ~/.profile
source ~/.profile
ansible-navigator
```
![Alt text](<1. install.JPG>)

![Alt text](<1. result.JPG>)

During Redhat exam : 
```
podman login
yum install ansible ansible-navigator
```
Get list of image repos by running : `podman images`
```
REPOSITORY                  TAG         IMAGE ID      CREATED       SIZE
ghcr.io/ansible/creator-ee  v0.22.0     4405e824c556  3 months ago  869 MB
```
Create config file of navigator, paste recent image link :
```
vim /home/sandy/ansible/ansible-navigator.yml
```
```
---
ansible-navigator:
  execution-environment:
    image: ghcr.io/ansible/creator-ee:v0.22.0
    pull:
      policy: missing
  playbook-artifact:
    enable: false
```
Test this config by running: 
```
ansible-navigator
```
# Create a file called packages.yml in /home/sandy/ansible to install packages httpd, mod_ssl, and mariadb.
Create playbook:
```
vim /home/sandy/ansible/packages.yml
```
```
---
- name: install apps
  hosts: test
  tasks:
    - name: install httpd, mod_ssl, mariadb
      yum:
        name:
          - httpd
          - mod_ssl
          - mariadb
          - mariadb-server
        state: latest
    - name: start httpd
      service:
        name: httpd
        state: started
    - name: start mariadb
      service:
        name: mariadb
        state: started
    - name: add firewall port
      firewalld:
        service: http
        permanent: true
        state: enabled
```
Test syntax and simulate running. You may encounter sudoer permission problem. If so, edit ansible.cfg and change become_user to root.
```
ansible-playbook packages.yml --syntax-check
ansible-playbook -v packages.yml --check
ansible-navigator run -m stdout packages.yml --check
```
Run playbook with navigator:
```
ansible-navigator run -m stdout packages.yml
```
![Alt text](<2. result run.gif>)

Check with adhoc:

![Alt text](<2. result.JPG>)

# Create a playbook that changes the default target on all nodes to multi-user target. Do this in playbook file called target.yml in/home/sandy/ansible

This operation creates link of `/usr/lib/systemd/system/multi-user.target` file in `/etc/systemd/system/default.target`. We can use `File` module for this task:
```
vim /home/sandy/ansible/target.yml
```

```
---
- name: target changer
  hosts: all
  tasks:
    - name: multi-user enabling
      ansible.builtin.file:
        src: /usr/lib/systemd/system/multi-user.target
        dest: /etc/systemd/system/default.target
        state: link

```
```
ansible-navigator run -v -m stdout target.yml
```
![Alt text](<3. result run.gif>)

Check with adhoc

![Alt text](<3. result.JPG>)

# Create a playbook called issue.yml in /home/sandy/ansible which changes the file /etc/issue on all managed nodes and write "Development"

```
vim /home/sandy/ansible/issue.yml
```

```
---
- name: file changer
  hosts: all
  tasks:
    - name: file editor
      ansible.builtin.copy:
        content: "Development"
        dest: /etc/issue


```
```
ansible-navigator run -v -m stdout issue.yml
```
![Alt text](<4. result run.gif>)

Check with adhoc

![Alt text](<4. result.JPG>)