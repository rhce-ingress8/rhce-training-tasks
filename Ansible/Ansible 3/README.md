# Tasks

1. Create vars directory in your project folder , use group_vars and host_vars example playbook

2. Create a playbook named playbook.yml. Create a play named "Deploy and start Apache HTTPD service", target the host group webserver as the managed hosts, and define the following variables in its vars_files section: web_pkg: httpd , frwl_pkg: firewalld, web_svc: httpd, frwl_svc: firewalld, rule: http

3. Install apache , ensure firewalld and apche service is started and enabled , and add apache service to firewall

4. Use extra variables in command line

----------------------------------------------
# TASK 1

 Create `vars` directory in your project folder , use `group_vars` and `host_vars` example playbook

Variables are widely used in scripts. For example, a=5, b=7. Desclared as a: 5, b: 7 in ansible. 
To pass variables and values inside hosts there are several ways:
We can directly declare them in playbook, or show location of variables file inside playbook. 

- Declaring as file:

Create directory:
```
mkdir /home/sandy/ansible/vars
```
Create variables file:
```
vim /home/sandy/ansible/vars/env.yml
a: 5
b: 7
```
Use this file:
```
vim /home/sandy/ansible/filevars.yml
---
- name: File variables
  hosts: prod
  vars_files:
    - vars/env.yml
  tasks:
  - name: debugger a
    debug:
      msg: a = {{ a }}
  - name: debugger b
    debug:
      msg: "{{ b }}"

```
Run playbook:
```
ansible-playbook filevars.yml
```

![Alt text](<1. file result.JPG>)

------------------------
Ansible has special folder structure for predefined values. You can automatically include variables for hosts and host groups without declaring them in playbook. 

![Alt text](<1. inventory.JPG>)

- group_vars. To pass values into host groups we need to create `group_vars` folder where ansible.cfg file resides. It requires valid file name renamed as groups in inventory file. 

For example we have dev, test, prod, proxy, webservers groups in inventory. Lets mark same variable with different values for host groups:  `srv_description` is our variable. 
```
mkdir /home/sandy/ansible/group_vars
cd /home/sandy/ansible/group_vars
touch dev test proxy prod 
echo "srv_description: Development server" > dev
echo "srv_description: Test server" > test
echo "srv_description: Proxy server" > proxy
echo "srv_description: Production server" > prod
cd ..
```
Now create playbook which shows value of variable across all servers:
```
vim groupvar.yml
---
- name: group variables
  hosts: all
  tasks:
    - name: Describe Server
      debug:
        msg: Server is {{ srv_description }}
```
Run playbook:
```
ansible-playbook groupvar.yml
```

![Alt text](<1. group result.JPG>)

- `host_vars`. To pass dedicated values into per host we need create `host_vars` folder and create files renamed as hostnames of inventory in this folder. Then we add needed variables in these files. 

For example we have node[1-5].example.com  hosts in inventory. Lets mark same variable with different values for each host :  `host_description` is our variable.

```
mkdir /home/sandy/ansible/host_vars
cd /home/sandy/ansible/host_vars
for i in 1 2 3 4 5 ; do echo "host_description: node_number_is_$i">node$i.example.com ; done
cd..
```
Now create playbook which shows value of variable across all servers:
```
vim hostvar.yml
---
- name: host variables
  hosts: all
  tasks:
    - name: Describe Hosts
      debug:
        var: host_description
```
Run playbook:
```
ansible-playbook hostvar.yml
```

![Alt text](<1. host result.JPG>)

# TASK 2

Create a playbook named playbook.yml. Create a play named "Deploy and start Apache HTTPD service", target the host group webserver as the managed hosts, and define the following variables in its vars_files section: web_pkg: httpd , frwl_pkg: firewalld, web_svc: httpd, frwl_svc: firewalld, rule: http

Create variables file:
```
vim /home/sandy/ansible/vars/apache.yml
```
```
web_pkg: httpd
frwl_pkg: firewalld
web_svc: httpd
frwl_svc: firewalld
rule: http
```
Create Playbook:
```
vim /home/sandy/ansible/playbook.yml
```
```
---
- name: Deploy and start Apache HTTPD service
  hosts: webserver
  vars_files:
    - vars/apache.yml
  tasks:
    - name: Install Apache on server
      ansible.builtin.yum:
        name: "{{web_pkg}}"
        state: latest

    - name: Install firewall
      ansible.builtin.yum:
        name: "{{frwl_pkg}}"
        state: latest

    - name: Start firewalld
      ansible.builtin.service:
        name: "{{frwl_svc}}"
        state: started

    - name: Add Http service to firewall
      ansible.posix.firewalld:
        service: "{{rule}}"
        permanent: true
        state: enabled

    - name: Start httpd
      ansible.builtin.service:
        name: "{{web_svc}}"
        state: started
    
    - name: Reload firewalld
      ansible.builtin.shell:
        cmd: firewall-cmd --reload

```

![Alt text](<2. deploy.JPG>)

![Alt text](<2. result.JPG>)

# TASK 3
- Install apache , ensure firewalld and apche service is started and enabled , and add apache service to firewall

Create Playbook:
```
vim /home/sandy/ansible/apache2.yml
```
```
---
- name: Deploy and start Apache HTTPD service
  hosts: webserver
  tasks:
    - name: Install Apache on server
      ansible.builtin.yum:
        name: httpd
        state: latest

    - name: Install firewall
      ansible.builtin.yum:
        name: firewalld
        state: latest

    - name: Start firewalld
      ansible.builtin.service:
        name: firewalld
        state: started
        enabled: yes

    - name: Add Http service to firewall
      ansible.posix.firewalld:
        service: http
        permanent: true
        state: enabled

    - name: Start httpd
      ansible.builtin.service:
        name: httpd
        state: started
        enabled: yes
    
    - name: Reload firewalld
      ansible.builtin.shell:
        cmd: firewall-cmd --reload
```
# TASK 4
Use extra variables in command line

For quick checking variables we use extra vars. These are highest privileged variable input. 
```
ansible-playbook filevars.yml -e 'a=11 b=30'
```

![Alt text](<4. result.JPG>)

