https://go2docs.graylog.org/5-0/downloading_and_installing_graylog/installing_graylog.html

# on Redhat 

Graylog 5.0 requires the following to maintain compatibility with its software dependencies: 

```
OpenJDK 17 (This is embedded in Graylog 5.0 and does not need to be separately installed.)
OpenSearch 1.x, 2.x (or Elasticsearch 7.10.2)
MongoDB 5.x or 6.x
```


1. MongoDB
First, add the repository file /etc/yum.repos.d/mongodb-org.repo with the following contents:

```
[mongodb-org-6.0]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/6.0/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-6.0.asc
```


After that, install the latest release of MongoDB:

```
sudo yum install -y mongodb-org
```

Enable MongoDB during the operating system’s startup and verify it is running:
```
sudo systemctl daemon-reload
sudo systemctl enable mongod
sudo systemctl start mongod
sudo systemctl status mongod
```


2. Elasticsearch
Elasticsearch 7.10.2 is the only version that is compatible with Graylog 5.0; however, we recommend OpenSearch for new Graylog cluster installations.

First, install the Elasticsearch GPG key:
```
rpm --import https://artifacts.elastic.co/GPG-KEY-elasticsearch
```

Then add the repository file /etc/yum.repos.d/elasticsearch.repo with the following contents:

```
sudo rpm --import https://artifacts.elastic.co/GPG-KEY-elasticsearch
```

```
echo "[elasticsearch-7.10.2]
name=Elasticsearch repository for 7.10.2 packages
baseurl=https://artifacts.elastic.co/packages/oss-7.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=1
autorefresh=1
type=rpm-md" | sudo tee /etc/yum.repos.d/elasticsearch.repo
```


```
sudo yum install elasticsearch-oss
sudo rpm install elasticsearch-oss
```


Modify the Elasticsearch configuration file (`/etc/elasticsearch/elasticsearch.yml`), set the `cluster name to graylog`, and uncomment `action.auto_create_index: false` to enable the action

 After you have modified the configuration, you can start Elasticsearch:

```
sudo systemctl daemon-reload
sudo systemctl enable elasticsearch.service
sudo systemctl restart elasticsearch.service
sudo systemctl --type=service --state=active | grep el
```


3. Graylog
Now install the Graylog repository configuration and Graylog Open itself with the following commands:
```
sudo rpm -Uvh https://packages.graylog2.org/repo/packages/graylog-5.0-repository_latest.rpm
sudo yum install graylog-server
```


If you are installing Graylog Operations, then you will use the following commands:
```
sudo rpm -Uvh https://packages.graylog2.org/repo/packages/graylog-5.0-repository_latest.rpm
sudo yum install graylog-enterprise
```


4. Edit the Configuration File  
Read the instructions within the configurations file and edit as needed, located at `/etc/graylog/server/server.conf`. Additionally add `password_secret` and `root_password_sha2` as these are mandatory and Graylog will not start without them.

To create your `password_secret`, run the following command: This is database password
```
< /dev/urandom tr -dc A-Z-a-z-0-9 | head -c${1:-96};echo;
```

To generate a `root_password_sha2`: web admin password
```
echo -n "Enter Password: " && head -1 </dev/stdin | tr -d '\n' | sha256sum | cut -d" " -f1
```

Write result to `server.conf` after `password_secret` and `root_password_sha2`.

Start service
```
sudo systemctl daemon-reload
sudo systemctl enable graylog-server.service
sudo systemctl start graylog-server.service
sudo systemctl --type=service --state=active | grep graylog
```


5. Connect to the Web Interface
In `server.conf` add you ip, and restart service
```
http_bind_address 192.168.56.60:9000
```
```
sudo systemctl restart graylog-server.service
firewall-cmd --add-port=9000/tcp --permanent
firewall-cmd --add-port=9000/tcp
```
