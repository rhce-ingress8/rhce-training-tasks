# Tasks

1. Install and Configure Graylog server, Mongodb, Elasticsearch

2. Create user, add role

3. Configure new 2 input

3.1 1st input for haproxy.Configure haproxy logs redirect to graylog

3.2 2nd input for all vm.Redirect logs to graylog and Add stream , stream rule

4. Configure notifications to send alerts to the Telegram group

5. Create Content Pack

# Optional

1. Install and configure Gitlab Server

1.1 Create new repo and push to gitlab

2. Install NTP server

2.1 Sync client with NTP server