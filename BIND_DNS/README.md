master 		192.168.56.11	
slave		192.168.56.12	

| IP address | test.local | ingress.local |
| ------ | ------ | ------ |
|    192.168.56.11    |    Prim    |   Sec     |
|    192.168.56.12    |     Sec   |    Prim    |


_____TASKS_________
1. Install Bind and utils
2. ADD 2 different zone and test it to resolve any hostname
3. Configure Master and Slave,
Make first zone master on one host and second on different host
4. Configure Reverse zone and test it dig -x “IP address” should return the domain name
5. Map one domain name to another
6. After each change in the master zone file, the entries must be copied to the slave dns


#_____MASTER________			{test.local}
1. Install DNS (BIND)

```
yum -y install bind bind-utils  
systemctl enable --now named
```


2. Configure DNS (BIND) 

`vim /etc/named.conf`
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

```
options {

	listen-on port 53 { 127.0.0.1; 192.168.56.11;};		#1 interface of master
	allow-query       { localhost; 192.168.56.0/24;};	#2 served network
	allow-transfer { localhost; 192.168.56.12; };		#3 allowed zone replica
	recursion yes;						#4 type of dns service
	include "/etc/test.local.named.conf";			#5 separate zone cfg
	notify yes;
	also-notify { 192.168.56.12;};				#6 auto replicate slave
```

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

3. Zone separate config file 

`vim /etc/test.local.named.conf`

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

```
zone "test.local" IN {						# fwd zone

	type master;						# master mark
	file "/var/named/fwd.test.local.db";			# fwd zone file location
	allow-update { none; };					# no need to upd on master
};
```


```
zone "56.168.192.in-addr.arpa." IN {				# rev zone

        type master;						# master mark
        file "/var/named/rev.test.local.db";			# rev zone file location
        forwarders {};						
};
```


+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

4. Zone file yaradilir

```
cd /var/named/

cp named.empty fwd.test.local.db

cp named.empty rev.test.local.db

vim fwd.test.local.db						# fwd zone file
```


+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

```
$TTL 3H
@       IN SOA @ test.local. (
                                        001     ; serial
                                        1D      ; refresh
                                        1H      ; retry
                                        1W      ; expire
                                        3H )    ; minimum
        NS      @
        A       127.0.0.1
        AAAA    ::1

@          IN         NS master.test.local.  ; masterdns
@          IN         NS slave.test.local.   ; slavedns

master     IN         A  192.168.56.11
slave      IN         A  192.168.56.12
client     IN         A  192.168.56.13
```


+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

`vim rev.test.local.db`						# rev zone file

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

```
$TTL 3H
@       IN SOA  @ test.local. (
                001     ; serial
                1M      ; refresh
                1H      ; retry
                1W      ; expire
                3H )    ; minimum
;       owner   TTL     CL      type    RDATA
                600     IN      NS     master.test.local.
                600     IN      NS     slave.test.local.
11     IN      PTR     master.test.local.
12     IN      PTR     slave.test.local.
13     IN      PTR     client.test.local.
```


+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

5. Test & Troubleshoot

named-checkconf							# Config test

named-checkzone test.local. fwd.test.local.db			# fwd zone test

named-checkzone 56.168.192.in-addr.arpa rev.test.local.db	# rev zone test

TROUBLESHOOTING 

systemctl enable --now named					# enable service

chown named:named -R /var/named					# owner of file

nmcli con mod enp0s3 ipv4.dns 192.168.56.11			# ipv4 Primary DNS on master

vi /etc/resolv.conf	nameserver 192.168.56.11

systemctl restart NetworkManager

restorecon -rv /var/named					# Selinux 

chcon system_u:object_r:named_conf_t:s0 /etc/test.local.named.conf

chcon system_u:object_r:named_zone_t:s0 /var/named/fwd.test.local.db

chcon system_u:object_r:named_zone_t:s0 /var/named/rev.test.local.db

systemctl restart named

firewall-cmd --add-service=dns --permanent			# firewall ports

firewall-cmd --reload

systemctl restart named

nslookup master.test.local					# resolve test

nslookup 192.168.56.11


#_____SLAVE________			{test.local}

1. Install DNS (BIND)

```
yum -y install bind bind-utils  
systemctl enable --now named
```


2. Configure DNS (BIND)  

`vim /etc/named.conf`

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

```
options {

	listen-on port 53 { 127.0.0.1; 192.168.56.12;};		# interface of server
	allow-query       { localhost; 192.168.56.0/24;};	# served network
	recursion yes;
	include "/etc/test.local.named.conf";			# separate config
	allow-notify {192.168.56.11;};				# permission to accept replica push from master
```


+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

3. Zone separate config file 

`vim /etc/test.local.named.conf`

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

```
zone "test.local" IN {						# fwd zone

	type slave;						# slave mark
	file "/var/named/slaves/test.local.fwd.zone";		# replica location
	masters { 192.168.56.11; };				# master ip
	masterfile-format text;					# plain text format
};
```


```
zone "56.168.192.in-addr.arpa" IN {				# rev zone

	type slave;						# slave mark
	file "/var/named/slaves/test.local.rev.zone";		# replica location
	masters { 192.168.56.11; };				# master ip
	masterfile-format text;					# plain text format
};
```

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

5. Test & Troubleshoot

systemctl enable --now named					# enable service

nmcli con mod enp0s3 ipv4.dns 192.168.56.12			# ipv4 

Primary DNS on slave
								# Selinux 

chcon system_u:object_r:named_conf_t:s0 /etc/test.local.named.conf

firewall-cmd --add-service=dns --permanent			# 

firewall ports

firewall-cmd --reload

systemctl restart named

nslookup master.test.local					# resolve test

nslookup 192.168.56.11
