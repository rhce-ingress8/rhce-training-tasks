Tomcat SSL standard method
Copy chain and host cert to tomcat base. Now we make ssl redirect for one of instances:8089 catalog
vi conf/server.xml		#std

    <Connector port="8089" protocol="HTTP/1.1"
               connectionTimeout="20000"
               redirectPort="8449"
               maxParameterCount="1000"
            />
    <Connector port="8449" protocol="org.apache.coyote.http11.Http11AprProtocol"
               maxThreads="150" SSLEnabled="true"
               maxParameterCount="1000"
               >
        <UpgradeProtocol className="org.apache.coyote.http2.Http2Protocol" />
        <SSLHostConfig>
            <Certificate certificateKeyFile="/etc/pki/tls/private/master.key.pem"
                         certificateFile="/opt/tomcat/master.cert.pem"
                         certificateChainFile="/opt/tomcat/ca-chain.pem"
                         type="RSA" />
        </SSLHostConfig>
    </Connector>

This config requires APR

Install prereqs
yum install make gcc openssl libssldev openssl-devel

Download apr and install:
https://apr.apache.org/download.cgi

tar -xzf  apr-1.5.2.tar.gz
cd apr-1.5.2/
./configure
make
make install

Check apr installation
ls /usr/local/apr/lib/libapr-1.*

Check java version. You have to get result higher than 1.8 version

Download Tomcat Natives
https://tomcat.apache.org/download-native.cgi

tar -xf tomcat-native-2.0.7-src.tar.gz
cd tomcat-native-2.0.7-src/native/
./configure --with-apr=/usr/local/apr --with-java-home=$JAVA_HOME
make
make install

Check Natives installation
ll /usr/local/apr/lib/libtcnative-2.so

Set variables
vi .bash_profile
LD_LIBRARY_PATH='$LD_LIBRARY_PATH:/usr/local/apr/lib'

Not Working