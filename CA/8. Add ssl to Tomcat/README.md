Now tomcat private and public key and cert chain will be unified under one file

ca.cert.pem
intermediate.cert.pem
master.cert.pem
master.key.pem

# Creating bundle
```
cat master.cert.pem | tee -a test.local.bundled.crt
cat intermediate.cert.pem | tee -a test.local.bundled.crt
cat ca.cert.pem | tee -a test.local.bundled.crt
```


# Creating chain
```
cat intermediate.cert.pem > ca-chain.pem
cat ca.cert.pem >> ca-chain.pem
```


# Creating PFX
```
openssl pkcs12 -export -out master.full.pfx  -inkey /etc/pki/tls/private/master.key.pem -in test.local.bundled.crt

# or
openssl pkcs12 -export -out master.full.pfx -inkey /etc/pki/tls/private/master.key.pem -in ca.cert.pem -in intermediate.cert.pem -in master.cert.pem
```


# Tomcat SSL pfx method
Copy master.full.pfx to tomcat base & Edit server.xml
```
vi conf/server.xml		#pfx

  <Connector 
              port="8443" minProcessors="5" maxProcessors="75"
              enableLookups="true" disableUploadTimeout="true" 
              acceptCount="100"  maxThreads="200"
              scheme="https" secure="true" SSLEnabled="true"
              keystoreFile="/opt/tomcat/master.full.pfx"
              keystorePass="1234" 
              keystoreType="PKCS12" />
```


# Tomcat SSL standard method
Copy chain and host cert to tomcat base. Now we make ssl redirect for one of instances:8089 catalog
```
vi conf/server.xml		#std

    <Connector port="8089" protocol="HTTP/1.1"
               connectionTimeout="20000"
               redirectPort="8449"
               maxParameterCount="1000"
            />
    <Connector port="8449" protocol="org.apache.coyote.http11.Http11AprProtocol"
               maxThreads="150" SSLEnabled="true"
               maxParameterCount="1000"
               >
        <UpgradeProtocol className="org.apache.coyote.http2.Http2Protocol" />
        <SSLHostConfig>
            <Certificate certificateKeyFile="/etc/pki/tls/private/master.key.pem"
                         certificateFile="/opt/tomcat/master.cert.pem"
                         certificateChainFile="/opt/tomcat/ca-chain.pem"
                         type="RSA" />
        </SSLHostConfig>
    </Connector>
```

This config requires APR

For both cases:
```
vi conf/web.xml

<security-constraint>
    <web-resource-collection>
        <web-resource-name>Protected Context</web-resource-name>
        <url-pattern>/*</url-pattern>
    </web-resource-collection>
   <user-data-constraint>
       <transport-guarantee>CONFIDENTIAL</transport-guarantee>
   </user-data-constraint>
 </security-constraint>
```
