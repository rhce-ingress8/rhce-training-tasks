Combine server certificate in a bundle

cat master.cert.pem | tee -a test.local.bundled.crt
cat intermediate.cert.pem | tee -a test.local.bundled.crt
cat ca.cert.pem | tee -a test.local.bundled.crt

change .pem extension to .crt in windows, import root to trusted.