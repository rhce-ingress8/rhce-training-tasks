# Create new server certificate on LEMP server and sign it on Intermed.

```
yum install openssl mod_ssl -y
cd /etc/pki/tls
```


```
# private key: 
openssl genrsa -out private/test.az.key.pem 2048
# CSR: CN-test.az
openssl req -config openssl.cnf  -key private/test.az.key.pem -new -sha256 -out test.az.csr.pem 
```


```
# Copy server CSR to Intermed and Sign
openssl ca -config openssl.cnf -extensions server_cert  -days 375 -notext -md sha256 -in csr/test.az.csr.pem -out certs/test.az.cert.pem
```


# Make Bundle. 
```
cat test.az.cert.pem | tee -a test.az.bundled.crt
cat intermediate-iamengineer.cert.pem | tee -a test.az.bundled.crt
cat ca-iamengineer.cert.pem | tee -a test.az.bundled.crt
```


# Modify nginx config to redirect to SSL

```
server {
    listen       80;
    server_name  test.az;

    access_log /var/log/nginx/ingress_access.log ;
    error_log /var/log/nginx/ingress_error.log ;
    server_tokens off ;
# Add this line to redirect to 443
    return 301 https://$host$request_uri;


    location /sample {
        proxy_pass http://mylb ;
    }

}
# Settings for a TLS enabled server.
#
server {
        listen       443 ssl ;
        listen       [::]:443 ssl ;
        server_name  test.az;
#        root         /usr/share/nginx/html;
#
        ssl_certificate "/etc/pki/tls/certs/test.az.bundled.crt";
        ssl_certificate_key "/etc/pki/tls/private/test.az.key.pem";
        ssl_session_cache shared:SSL:1m;
        ssl_session_timeout  10m;
        ssl_ciphers PROFILE=SYSTEM;
        ssl_prefer_server_ciphers on;

    location /sample {
        proxy_pass http://mylb ;
    }

}

    upstream mylb {
        server 192.168.56.11:8089;
        server 192.168.56.11:8088;
        server 192.168.56.11:8087;
    }

```
Result: 
![Alt text](ssl_ng.gif)