Revoking cert & refreshing CRL

On Intermed:
openssl ca -config openssl.cnf -revoke certs/user02.cert.pem
openssl ca -config openssl.cnf -gencrl -out crl/intermediate.crl.pem