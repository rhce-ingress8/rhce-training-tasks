LAB Components:
192.168.56.11 - tomcat server
192.168.56.12 - intermed ca
192.168.56.13 - root ca

1. Install reguired package
2. Create root certificate and intermediate certificate
3. Sign server certificate and 2 user certificate
4. Revoke one of the user certificate
5. Send some screenshot below on windows machine CRL and server certificate
6. Add root certificate to Mozilla trusted authorities
7. Add ssl certificate to nginx and send config , screenshot https:// without any errors
8. Add ssl to Tomcat , send config and screenshot https:// without any errors
8.1. Configure with pfx file
8.2. Configure with standart method
9. Send me your chain and write what it consists of