# On root: - rootca.test.local - 56.13

First we need to create folder structure

```
mkdir /CA_ROOT
cd /CA_ROOT
mkdir conf certs crl newcerts private intermediate csr
chmod 400 private		#secure private key
```


Copy config file & create main files
```
cp /etc/pki/tls/openssl.cnf /CA_ROOT/
touch index.txt
echo 1000 > serial
echo 1000 > crlnumber
```


Make needed changes to config file

`vi openssl.cnf`
_________________________
```
[ CA_default ]
dir             	= /CA_ROOT			#redirect to custom folder
certificate     	= $dir/certs/ca.cert.pem	#root public key
private_key 		= $dir/private/ca.key.pem	#root private key

[ v3_ca ]
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always,issuer
basicConstraints = critical, CA:true
keyUsage = critical, digitalSignature, cRLSign, keyCertSign

[ v3_intermediate_ca ]
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always,issuer
basicConstraints = critical, CA:true, pathlen:0
keyUsage = critical, digitalSignature, cRLSign, keyCertSign

[ usr_cert ]
basicConstraints = CA:FALSE
nsCertType = client, email
nsComment = "OpenSSL Generated Client Certificate"
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer
keyUsage = critical, nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage = clientAuth, emailProtection

[ server_cert ]
basicConstraints = CA:FALSE
nsCertType = server
nsComment = "OpenSSL Generated Server Certificate"
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer:always
keyUsage = critical, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth
```

__________________________________

# Create Root certificates

```
#private key: passwd-12345
openssl genrsa -aes256 -out private/ca.key.pem 4096

#public key: CN-rootca.test.local
openssl req -config openssl.cnf -key private/ca.key.pem -new -x509 -days 7300 -sha256 -extensions v3_ca -out certs/ca.cert.pem
```

___________________________________

# Create Intermed certificate

On Intermed server: - intermed.test.local - 56.12

`yum install openssl mod_ssl -y`

First we need to create folder structure

```
mkdir /CA_INT
cd /CA_INT
mkdir conf certs crl newcerts private intermediate csr
chmod 400 private		#secure private key
touch index.txt
echo 2000 > serial 
echo 2000 > crlnumber
```


Copy openssl.cnf from root and edit it
```
[ CA_default ]
dir             	= /CA_INT		#redirect to custom folder
certificate     	= $dir/certs/intermediate.cert.pem	#intermed public key
private_key 		= $dir/private/intermediate.key.pem	#intermed private key
```


```
# private key: passwd-1234
openssl genrsa -aes256 -out private/intermediate.key.pem 4096
# CSR: CN-intermed.test.local
openssl req -config  openssl.cnf -new -sha256  -key private/intermediate.key.pem   -out csr/intermediate.csr.pem
```


# Sign CSR on Root CA
Copy intermed CSR to CSR folder on Root and run:

`openssl ca -config openssl.cnf -extensions v3_intermediate_ca  -days 3650 -notext -md sha256  -in csr/intermediate.csr.pem  -out certs/intermediate.cert.pem`

Copy generated intermed public key to intermed certs folder