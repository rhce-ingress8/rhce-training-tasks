# Server Cert:

On server - master.test.local - 56.11
```
yum install openssl mod_ssl -y
cd /etc/pki/tls
```


```
# private key: 
openssl genrsa -out private/master.key.pem 2048
# CSR: CN-master.test.local
openssl req -config openssl.cnf  -key private/master.key.pem -new -sha256 -out master.csr.pem 
```


```
# Copy server CSR to Intermed and Sign
openssl ca -config openssl.cnf -extensions server_cert  -days 375 -notext -md sha256 -in csr/master.csr.pem -out certs/master.cert.pem
```


Copy generated master.cert.pem to Certs folder on Server.

_________________________________

# User certificates

On any client
```
# private key:
openssl genrsa -out user01.key.pem 2048 
openssl genrsa -out user02.key.pem 2048 

# CSR: CN: user01.test.local, user02.test.local
openssl req -new -key user01.key.pem -out user01.csr.pem
openssl req -new -key user02.key.pem -out user02.csr.pem

# Copy CSR to Intermed CSR folder and Sign
openssl ca -config openssl.cnf -extensions usr_cert -days 375 -notext -md sha256 -in csr/user01.csr.pem -out certs/user01.cert.pem
openssl ca -config openssl.cnf -extensions usr_cert -days 375 -notext -md sha256 -in csr/user02.csr.pem -out certs/user02.cert.pem
```


Copy generated cert files to client
